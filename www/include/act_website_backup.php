<?php
class website_backup extends App
{
    function init()
    {
        $this->appInit(get_class($this));
        require_once("account_manage.php");
        $id = $_GET["2"];
        $result = mysql_query("SELECT * FROM vhosts WHERE id='" . $id . "'");
        $row = mysql_fetch_assoc($result);

        if ($row["owner_id"] != $_SESSION["user_id"] AND !SUPERVISOR) {
            go_home();
        } // if not owner of this site

        $website_domain = strtolower($row["servername"]);
        $new_website_domain_cleared = str_replace("\.", "_", $website_domain);
        $new_website_i_domain = $new_website_domain_cleared . "." . HOST_NAME;

        $this->_tpl->assignArray(array(
            "SITE_ID" => $id,
            "WEBSITE_DOMAIN" => $website_domain,
            "WEBSITE_URL" => $new_website_i_domain,
        ));

        $_chk = new AppCheck();

        if (!$_POST) // If nothing post, then NO ERRORS NO RESULTS
        {
            $this->_tpl->clearSection("is_error", $_GET["1"]);
            $this->_tpl->clearSection("no_error", $_GET["1"]);
        }


        if ($_POST["sendform"]) {


            $_chk->parse_msg($this->_tpl, "row_MSG", "MESSAGES");
            if ($_chk->messages == false) { //IF ERRORS NOT PRESENT

                $this->_tpl->clearSection("is_error", $_GET["1"]);
                $this->_tpl->clearSection("main", $_GET["1"]);
                $files = website_backup($id);
                $ftp_backup = "ftp://" . MAIN_PREFIX . $id . ":" . $row["ftp_password"] . "@" . HOST_NAME . "/" . $files["backup_name"];
                $ftp_mysqldump = "ftp://" . MAIN_PREFIX . $id . ":" . $row["ftp_password"] . "@" . HOST_NAME . "/" . $files["mysqldump_name"];

                    $this->_tpl->assignArray(array(

                        "LINK_BACKUP_FILENAME" => $ftp_backup,
                        "BACKUP_FILENAME" => $files["backup_name"],
                        "BACKUP_FILESIZE" => round($files["backup_size"] / 1024 / 1024, 2),
                        "LINK_MYSQLDUMP_FILENAME" => $ftp_mysqldump,
                        "MYSQLDUMP_FILENAME" => $files["mysqldump_name"],
                        "MYSQLDUMP_FILESIZE" => round($files["mysqldump_size"] / 1024 / 1024, 2),

                    ));


            } else $this->_tpl->clearSection("no_error", $_GET["1"]);


        }


    }
}

?>