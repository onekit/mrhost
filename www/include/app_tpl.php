<?php
require("fasttemplate.php");

class AppTpl extends FastTemplate
{

    // {{{ constructor
    /**
     * Constructor.
     *
     */
    function AppTpl($templateDir)
    {
        // call parent constructor
        $this->FastTemplate($templateDir);
    }

    // }}}

    // {{{ destructor
    /**
     * Destructor.
     *
     * @access public
     */
    function _AppTpl()
    {
        $this->clear_all();
    }

    // }}}

    // {{{ defineArray
    /**
     * DefineArray.
     * defines templates,
     *
     * @access public
     */
    function defineArray($tpl_array)
    {
        $this->define($tpl_array);
    }

    // }}}

    // {{{ defineOne
    /**
     * DefineOne.
     * Defines one template,
     *
     * @access public
     */
    function defineOne($tplname, $value)
    {
        $this->define(array($tplname => $value));
    }

    // }}}

    // {{{ defineDynamic
    /**
     * DefineDynamic.
     * Defines dynamic block,
     *
     * @access public
     */
    function defineDynamic($Macro, $ParentName)
    {
        $this->define_dynamic($Macro, $ParentName);
    }

    // }}}

    // {{{ clearDynamic
    /**
     * Clears template dynamic
     *
     * @access public
     */
    function clearDynamic($SectName, $TplName = null)
    {
        $this->clear_dynamic($SectName);
    }

    // }}}
    // {{{ defineSection
    /**
     * DefineSection.
     * Defines template section,
     *
     * @access public
     */
    function defineSection($SectName, $TplName)
    {
        $this->define_section($SectName, $TplName);
    }

    // }}}

    // {{{ clearSection
    /**
     * DefineSection.
     * Clears template section, defines it, if needed
     *
     * @access public
     */
    function clearSection($SectName, $TplName = null)
    {
        if ($TplName != null) {
            $this->define_section($SectName, $TplName);
        }
        $this->clear_section($SectName);
    }

    // }}}

    // {{{ assignArray
    /**
     * AssignArray.
     * assigns values to template variables,
     *
     * @access public
     */
    function assignArray($tpl_array, $trailer = "")
    {
        $this->assign($tpl_array, $trailer);
    }

    // }}}

    // {{{ assignOne
    /**
     * AssignOne.
     * assigns value to one template variable,
     *
     * @access public
     */
    function assignOne($varname, $value, $trailer = "")
    {
        $this->assign(array($varname => $value), $trailer);
    }

    // }}}

    // {{{ parseTpl
    /**
     * ParseTpl.
     * parses template,
     *
     * @access public
     */
    function parseTpl($ReturnVar, $FileTags)
    {
        $this->parse($ReturnVar, $FileTags);
    }

    // }}}

    // {{{ printTpl
    /**
     * printTpl.
     * prints parsed template,
     *
     * @access public
     */
    function printTpl($tplname)
    {
        $this->FastPrint($tplname);
    }
    // }}}
}

?>