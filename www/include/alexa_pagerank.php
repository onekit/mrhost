<?php

function get_alexa_rank($domain)
{
    $xpath = new DOMXPath(@DOMDocument::loadHTMLFile(
        sprintf(
            'http://www.alexa.com/siteinfo/%s',
            $domain
        )
    ));
    return $xpath->query("//div[@class='data down']")->item(0)->nodeValue; #1,023
}

function getAlexaData($url)
{
    $data = @file_get_contents('http://data.alexa.com/data?cli=10&dat=s&url=' . $url);
    if (empty($data)) return false;
    //echo $data;
    preg_match('/<POPULARITY.*?TEXT="([\d]+)"/i', $data, $_rank);
    preg_match('/<LINKSIN.*?NUM="([\d]+)"/i', $data, $_lnum);
    preg_match('/<SPEED.*?TEXT="([\d]+)".*?PCT="([\d]+)"/i', $data, $_speed);
    preg_match('/<CHILD.*?SRATING="([\d]+)"/i', $data, $_adult);
    preg_match('/<LANG.*?LEX="(.+?)"/i', $data, $_lang);
    preg_match('/<CREATED.*?DATE="(.+?)"/i', $data, $_created);

    $data = array();
    $data['POPULARITY'] = (int)$_rank[1];
    $data['LINKSIN'] = (int)$_lnum[1];
    $data['SPEED'] = round(($_speed[1] / 1000), 4);
    $data['SPEED_PCT'] = (int)$_speed[2];
    $data['CHILD'] = (int)$_adult[1];
    $data['LANG'] = $_lang[1];
    $data['CREATED'] = $_created[1];

    return $data;
}

?>