<?php
class password_reminder extends App
{
    function init()
    {
        $this->appInit(get_class($this));
        $_chk = new AppCheck();


        $this->_tpl->assignArray(array(
            "KEYWORDS" => "password, forgot",
            "DESCRIPTION" => "Forgot Password?"
        ));

        if (!$_POST) {
            $this->_tpl->clearSection("is_error", $_GET["1"]);
            $this->_tpl->clearSection("no_error", $_GET["1"]);
            return false;
        }
        if (!$_POST["login"]) {
            $_chk->add_msg(ERR_PASSWORD_REMINDER);
            $this->_tpl->assignArray(array("IS_ERROR_FORGOT_PASSWORD" => "error"));
            $this->_tpl->clearSection("no_error", $_GET["1"]);
            $_chk->parse_msg($this->_tpl, "row_MSG", "MESSAGES");
            return false;
        }

        $row = mysql_fetch_assoc(mysql_query("SELECT email, password FROM users WHERE login = '" . $_POST["login"] . "'"));
        if (!$row["email"]) {
            $_chk->add_msg(ERR_PASSWORD_REMINDER_2);
            $this->_tpl->assignArray(array("IS_ERROR_FORGOT_PASSWORD" => "error"));
            $this->_tpl->clearSection("no_error", $_GET["1"]);
            $_chk->parse_msg($this->_tpl, "row_MSG", "MESSAGES");
            return false;
        }


        $this->_tpl->clearSection("is_error", $_GET["1"]);
        $this->_tpl->clearSection("main", $_GET["1"]);
        $this->_tpl->assignArray(array(
            "LOGIN" => $_POST["login"],
        ));

///EMAIL TO CUSTOMER with forgotten password
        $message = "
PASSWORD REMINDER
============================

Login: " . $_POST["login"] . "
Password: " . $row["password"];
        mail($row["email"], HOSTING_TITLE . ": password reminder for " . $_POST['login'], $message, "From: \"" . HOSTING_TITLE . "\" <no-reply@" . HOST_NAME . ">");
// end EMAIL

    }

}

?>