<?php
class website_renew extends App
{
    function init()
    {
        $this->appInit(get_class($this));
        $id = $_GET["2"];
        $required_balance = STOCK_PRICE;
        $result = mysql_query("SELECT * FROM vhosts WHERE id='" . $id . "'");
        $row = mysql_fetch_assoc($result);

        if ($row["owner_id"] != $_SESSION["user_id"] AND !SUPERVISOR) {
            go_home();
        } // if not owner of this site

        $website_domain = $row["servername"];
        $website_a_domain = "www." . $website_domain;
        $website_domain_cleared = str_replace("\.", "_", $website_domain);
        $website_i_domain = $website_domain_cleared . "." . HOST_NAME;

        $this->_tpl->assignArray(array(

            "SITE_ID" => $id,
            "WEBSITE_DOMAIN" => $website_domain,
            "WEBSITE_INTERNAL_DOMAIN" => $website_i_domain,
            "WEBSITE_ADDITIONAL_DOMAIN" => $website_a_domain,

        ));

        $_chk = new AppCheck();

        if (!$_POST) // If nothing post, then NO ERRORS NO RESULTS
        {
            $this->_tpl->clearSection("is_error", $_GET["1"]);
            $this->_tpl->clearSection("no_error", $_GET["1"]);
        }


        if ($_POST["sendform"]) {


            if (BALANCE < $required_balance) {
                $_chk->add_msg(ERR_BALANCE);
            }
            $_chk->parse_msg($this->_tpl, "row_MSG", "MESSAGES");

            if ($_chk->messages == false) { //IF ERRORS NOT PRESENT

                $this->_tpl->clearSection("is_error", $_GET["1"]);
                $this->_tpl->clearSection("main", $_GET["1"]);

                $sqry = "UPDATE users SET balance=balance-" . $required_balance . " WHERE id='" . $_SESSION["user_id"] . "'";
                mysql_query($sqry);
                //get 50 USD from balance

                $sqry = "UPDATE users SET balance=balance+10 WHERE id='" . USER_PARENT_ID . "'";
                mysql_query($sqry);
                //add 10 USD to reffer

                $sqry = "INSERT INTO transactions SET user_id_sender='" . $_SESSION["user_id"] . "', user_id_recipient='" . USER_PARENT_ID . "', amount='10', trans_date=NOW();";
                mysql_query($sqry);
                //log transaction


                $sqry = "UPDATE vhosts SET expdate = DATE_ADD(expdate,INTERVAL 1 YEAR) WHERE id = '" . $id . "';";
                mysql_query($sqry);

                //update notification flag
                $sqry = "UPDATE vhosts SET notify='0' WHERE id='" . $id . "';";
                mysql_query($sqry);


///EMAIL TO ADMIN 
                $message = "
http://" . $website_i_domain . "
was renewed.
Kind regards,
" . HOSTING_TITLE;

                mail(ADMIN_ADDRESS, "WEBSITE RENEWED: " . $website_domain, $message, "From: " . ADMIN_ADDRESS . "");
// end EMAIL 

//update virtual hosts config in Apache

                exec("/etc/apache2/update_vhosts");
                exec('echo "/usr/bin/sudo /etc/init.d/apache2 reload" | /usr/bin/at now'); //reload apache config (special rights)


                header("Location: /website_info/" . $id);
                exit;
            } else $this->_tpl->clearSection("no_error", $_GET["1"]);


        }


    }
}

?>