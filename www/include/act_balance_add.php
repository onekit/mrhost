<?php
class balance_add extends App
{
    function init()
    {
        $this->appInit(get_class($this));

        $_chk = new AppCheck();

        if (!TWO_CHECKOUT_SID  OR !TWO_CHECKOUT_PRODUCT_ID) {
            $this->_tpl->clearSection("two_checkout", $_GET["1"]);
        }

        if (@!$_POST) // If nothing post, then NO ERRORS NO RESULTS
        {
            $this->_tpl->clearSection("is_error", $_GET["1"]);
            $this->_tpl->clearSection("no_error", $_GET["1"]);
        }


        if (@$_POST["sendform"]) {


            if (!$_POST["pay_code"]) {
                $_chk->add_msg(ERR_PAY_CODE_EMPTY);
            }

            if ($_POST["pay_code"]) {


                $pay_code = $_POST["pay_code"];

                $result = mysql_query("SELECT * FROM pay_codes WHERE code='" . $pay_code . "' AND user_id_accept=0;");
                $row = mysql_fetch_assoc($result);
                $sqry = "UPDATE pay_codes SET user_id_accept='" . $_SESSION["user_id"] . "' WHERE code='" . $pay_code . "';";
                mysql_query($sqry); // update pay code (disable after action)

                $balance_give = $row["balance"] ? $row["balance"] : 0;

            }

            if (!$balance_give AND $_POST["pay_code"]) {
                $_chk->add_msg(ERR_PAY_CODE_WRONG);
            }


            $_chk->parse_msg($this->_tpl, "row_MSG", "MESSAGES");


            if ($_chk->messages == false) { //IF ERRORS NOT PRESENT

                $this->_tpl->clearSection("is_error", $_GET["1"]);
                $this->_tpl->clearSection("main", $_GET["1"]);

                $this->_tpl->assignArray(array(
                    "BALANCE_ADDED" => $balance_give
                ));

                $sqry = "UPDATE users SET balance = balance + " . $balance_give . " WHERE id='" . $_SESSION["user_id"] . "'";
                mysql_query($sqry);

                $notes = "Paycode " . $pay_code . " accepted";
                $sqry = "INSERT INTO transactions SET user_id_sender='" . $row["user_id_provider"] . "', user_id_recipient='" . $_SESSION["user_id"] . "', amount='" . $balance_give . "', notes='" . $notes . "', trans_date=NOW();";
                mysql_query($sqry);
                //log transaction

            } else $this->_tpl->clearSection("no_error", $_GET["1"]);


        }


    }
}

?>