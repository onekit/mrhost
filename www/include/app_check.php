<?php
class AppCheck
{

    var $messages = array();

    //***********************************************
    // Constructor
    function AppCheck($message = "")
    {
    }

    //***********************************************
    // Destructor
    function _AppCheck()
    {
    }

    //***********************************************
    function add_msg($message)
    {
        $this->messages[] = $message;
        $this->messages = array_unique($this->messages);
    }

    //***********************************************
    function clear_msg()
    {
        $this->messages = array();
    }

    //***********************************************
    function is_msg()
    {
        if ($this->messages) {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    //***********************************************
    function parse_msg(&$tpl, $dynamic_section, $row_msg)
    {
        if ($this->is_msg()) {
            $tpl->defineDynamic("$dynamic_section", $_GET["1"]);
            reset($this->messages);
            while (list ($key, $val) = each($this->messages))
            {
                if ($val) {
                    $tpl->assignArray(array("$row_msg" => $val));
                    $tpl->parse("null", ".$dynamic_section");
                }
            }
        }
    }

    //***********************************************
    function get_msg()
    {
        $str = "";

        if ($this->is_msg()) {
            reset($this->messages);
            while (list ($key, $val) = each($this->messages))
            {
                if ($val) {
                    $str .= $val . "<BR>";
                }
            }
        }
        return $str;
    }

    //***********************************************
    function check_pw($pw)
    {
        if (strlen(chop($pw)) < 5) {
            $this->add_msg(ERR_PASSWORD_LENGTH_INVALID);
            return FALSE;
        }
        return TRUE;
    }


    //***********************************************
    function check_email(&$email)
    {
        $return_flag = TRUE;
        $email = trim($email);

        if (strlen($email) == 0) {
            $this->add_msg(ERR_EMAIL_REQUIRED);
            return false;
        }

        if (!preg_match("/^[^@]*@[^@]*\.[^@]*$/", $email)) {
            $this->add_msg(ERR_EMAIL_INVALID);
            $return_flag = FALSE;
        }

        return $return_flag;
    }

    //***********************************************
    function check_phone(&$phone, $empty = false)
    {
        // must contain all legal characters
        $phone = trim($phone);
        if (strspn($phone, "0123456789-+();,. ") != strlen($phone)) {
            $this->add_msg(ERR_PHONE_INVALID);
            return FALSE;
        }
        if ($empty && (strlen($phone) < 1)) {
            $this->add_msg(ERR_PHONE_INVALID);
            return FALSE;
        }
        return TRUE;
    }

    //***********************************************
    function check_fax(&$fax)
    {
        // must contain all legal characters
        $fax = trim($fax);
        if (strspn($fax, "0123456789-+() ") != strlen($fax)) {
            $this->add_msg(ERR_FAX_INVALID);
            return FALSE;
        }
        return TRUE;
    }

    //***********************************************
    function check_numeric(&$data)
    {
        // must contain all legal characters
        $data = trim($data);
        if (strspn($data, "0123456789.") != strlen($data)) {
            $this->add_msg(ERR_INVALID_NUMERIC);
            return FALSE;
        }
        return TRUE;
    }

    //***********************************************
    function check_url(&$url)
    {
        $url = trim($url);
        if (substr($url, 0, 7) != "http://" && (strlen($url) != 0)) $url = "http://" . $url;

        if ((preg_match("/^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?/i", $url, $components) == 0) && (strlen($url))) {
            $this->add_msg(ERR_URL_INVALID);
            return FALSE;
        }
        return TRUE;
    }

    //***********************************************
    function check_org(&$org)
    {
        $org = trim($org);
        if (strlen($org) < 1) {
            $this->add_msg(ERR_ORG_INVALID);
            return false;
        }
        return TRUE;

    }

    //***********************************************
    function check_nif(&$nif)
    {
        // must contain all legal characters
        $nif = trim($nif);
        $nif1 = substr($nif, 0, 1);
        $nif2 = substr($nif, 1);
        if ((strlen($nif) != 9) || (strspn($nif1, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ") != strlen($nif1)) || (strspn($nif2, "0123456789") != strlen($nif2))) {
            $this->add_msg(ERR_NIF_INVALID);
            return FALSE;
        }
        return TRUE;
    }

    //***********************************************
    function check_zip(&$zip)
    {
        // must contain all legal characters
        $zip = trim($zip);
        if ((strspn($zip, "0123456789") != strlen($zip)) || (strlen($zip) < 5) || (strlen($zip) > 6)) {
            $this->add_msg(ERR_ZIP_INVALID);
            return FALSE;
        }

        return TRUE;
    }

    //***********************************************
    function check_address(&$address)
    {
        // must contain all legal characters
        $address = trim($address);
        if (strlen($address) > 0) return TRUE;
        $this->add_msg(ERR_INVALID_ADDRESS);
        return FALSE;
    }

    //***********************************************
    function check_city(&$city, $flag_lenght_check_required = true)
    {
        // must contain all legal characters
        $city = trim($city);
        /*if ((strspn($city,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789- ") != strlen($city)))		{
              $this->add_msg( ERR_CITY_INVALID);
              return FALSE;
          }*/
        if ($flag_lenght_check_required && (strlen($city) < 1)) {
            $this->add_msg(ERR_CITY_INVALID);
            return FALSE;
        }

        return TRUE;
    }

    //***********************************************
    function check_province(&$province)
    {
        // must contain all legal characters
        if (strspn($province, "0123456789") != strlen($province)) {
            $this->add_msg(ERR_PROVINCE_INVALID);
            return false;
        }
        return TRUE;
    }

    //***********************************************
    function check_contact(&$contact, $empty = false)
    {
        // must contain all legal characters
        $contact = trim($contact);
        /*if ((strspn($contact,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ") != strlen($contact)) || (strlen($contact) < 1))
          {
              $this->add_msg(ERR_PERSON_INVALID);
              return FALSE;
          }*/

        if ($empty && (strlen($contact) < 1)) {
            $this->add_msg(ERR_PERSON_INVALID);
            return FALSE;
        }
        return TRUE;
    }

    //***********************************************
    function check_image($fname, $ALLOW_IMG_FILES)
    {
        $imginfo = getimagesize($fname);
        if (!array_key_exists($imginfo[2], $ALLOW_IMG_FILES)) {
            $this->add_msg(ERR_IMG_SAVE3);
            return FALSE;
        }
        if ($imginfo[2] == 1) {
            if (filesize($fname) > IMG_FILE_MAX_SIZE) {
                $this->add_msg(sprintf(ERR_IMG_SAVE1, IMG_FILE_MAX_SIZE));
                return FALSE;
            }
            if ($imginfo[0] > IMG_MAX_X || $imginfo[1] > IMG_MAX_Y) {
                $this->add_msg(sprintf(ERR_IMG_SAVE2, IMG_MAX_X, IMG_MAX_Y));
                return FALSE;
            }
        }
        return TRUE;
    }
}

?>