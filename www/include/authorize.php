<?php
require_once("urling.php");
if (!isset($_SERVER['HTTP_REFERER'])) {
    $_SERVER['HTTP_REFERER'] = "";
}
$this->_tpl->assignArray(array(
    "HOST_NAME" => HOST_NAME,
    "HOST_IP" => HOST_IP,
    "HOSTING_TITLE" => HOSTING_TITLE,
    "STOCK_PRICE" => STOCK_PRICE,
    "SUPPORT_EMAIL" => SUPPORT_EMAIL,
    "MAIN_PREFIX" => MAIN_PREFIX,

));
$_SESSION["current_url"] = $_SERVER["REQUEST_URI"];
$_SESSION["previous_url"] = $_SERVER["HTTP_REFERER"];


function go_home()
{
    $_SESSION["pre_previous_url"] = $_SERVER["REQUEST_URI"];
    header("Location: //" . HOST_NAME . "/");
    exit;
}


// --- BEGIN OF LOGIN CHECK

if (!isset($_SESSION['login'])) {
    $this->_tpl->clearSection("logged_in_usr", "header");
    $this->_tpl->clearSection("logged_in_usr", $_GET["1"]);
    $this->_tpl->clearSection("logged_in_usr", "footer");
} else {
    $sqry = "SELECT * FROM users WHERE login='" . strtolower($_SESSION['login']) . "'";
    $authrow = mysql_fetch_assoc(mysql_query($sqry));

    if (strtolower($_SESSION['login']) != $authrow['login'] || $_SESSION['password'] != md5($authrow['password']) || !$authrow["active"]) {
        $this->_tpl->clearSection("logged_in_usr", "header");
        $this->_tpl->clearSection("logged_in_usr", $_GET["1"]);
        $this->_tpl->clearSection("logged_in_usr", "footer");
    } else {

        if (!$authrow["active"]) {
            $this->_tpl->clearSection("logged_in_usr", "header");
            $this->_tpl->clearSection("logged_in_usr", $_GET["1"]);
            header("Location: //" . HOST_NAME . "/login/out");
            exit; //logout rapidly
        }
        $this->_tpl->clearSection("logged_out", "header");
        $this->_tpl->clearSection("logged_out", $_GET["1"]);
        $this->_tpl->assignArray(array("LOGIN_ID" => $_SESSION['login']));
        $this->_tpl->assignArray(array("CREDITS" => $authrow["balance"]));
        $this->_tpl->assignArray(array("LOGGED_USERNAME" => $authrow["username"]));
        $this->_tpl->assignArray(array("USER_ID" => $_SESSION["user_id"]));

    }

}
// --- END OF LOGIN CHECK

//supervisor access (admin)
if (@SUPERVISOR == 1) {
    $this->_tpl->parseTpl("MENU_ADMIN", "menu_admin");
} else {
    $this->_tpl->clearSection("admin_functions", $_GET["1"]);
}

?>