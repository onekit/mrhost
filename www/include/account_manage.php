﻿<?php
function website_create($user_id, $master_domain)
{
    $owner_id = $user_id;
    mysql_select_db(DBNAME);

    $sqry = "INSERT INTO vhosts SET servername='" . $master_domain . "'";
    mysql_query($sqry);
    $last_id = mysql_fetch_assoc(mysql_query("SELECT * FROM vhosts WHERE servername='" . $master_domain . "'"));
    $id = $last_id["id"];

    $sqry = "INSERT INTO vhosts_info SET vhost_id='" . $id . "'";
    mysql_query($sqry); // add domain control

    $row = mysql_fetch_assoc(mysql_query("SELECT * FROM users WHERE id='" . $owner_id . "'"));
    $password = substr(md5(rand(0, 1000)), 0, 8);
    $email = $row["email"];

    $year = date("Y");
    $month = date("m");
    $day = date("d");
    $expdate = ($year + 1) . "-" . $month . "-" . $day;
    $username = $row["username"];

    mysql_select_db("ftpd");
    $ftp_dir = "/home/hosting/" . MAIN_PREFIX . $id;
    system("mkdir " . $ftp_dir);
    $htdocs_dir = "/home/hosting/" . MAIN_PREFIX . $id . "/htdocs"; //www folder
    system("mkdir " . $htdocs_dir);
    $tmp_dir = "/home/hosting/" . MAIN_PREFIX . $id . "/tmp"; //temp folder for uploaded files and sessions
    system("mkdir " . $tmp_dir);
    $logs_dir = "/home/hosting/" . MAIN_PREFIX . $id . "/logs"; // folder for apache log files
    system("mkdir " . $logs_dir);
    system("chmod 777 -R " . $ftp_dir);
    $sqry = "INSERT INTO users (user, password, home) VALUES ('" . MAIN_PREFIX . $id . "', MD5('" . $password . "'), '" . $ftp_dir . "');";
    mysql_query($sqry); //creating ftp user

    mysql_query("create database " . MAIN_PREFIX . $id . ";"); //add mysql database
    mysql_query("grant CREATE,INSERT,DELETE,UPDATE,SELECT,DROP on " . MAIN_PREFIX . $id . ".* to " . MAIN_PREFIX . $id . "@localhost;");
//add mysql user
//grant mysql user
    mysql_query("set password for " . MAIN_PREFIX . $id . "@localhost = password('" . $password . "');");
    mysql_query("flush privileges;");
//set password for mysql user

//create vhost 
    mysql_select_db(DBNAME);
    $website_domain_cleared = str_replace("\.", "_", $master_domain);
    $website_i_domain = $website_domain_cleared . "." . HOST_NAME;
    $vhost_container = "
<VirtualHost *>
ServerAdmin " . $email . "
DocumentRoot /home/hosting/" . MAIN_PREFIX . $id . "/htdocs
ServerName " . $master_domain . "
ServerAlias " . $website_i_domain . " www." . $master_domain . "
<Directory /home/hosting/" . MAIN_PREFIX . $id . "/htdocs>
Options Indexes FollowSymLinks MultiViews
AllowOverride All
Order allow,deny
allow from all
</Directory>

<Directory />
Options Indexes FollowSymLinks MultiViews
AllowOverride All
Order allow,deny
allow from all
</Directory>
	
ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
<Directory \"/usr/lib/cgi-bin\">
AllowOverride All
Options ExecCGI -MultiViews +SymLinksIfOwnerMatch
Order allow,deny
Allow from all
</Directory>

ErrorLog /home/hosting/" . MAIN_PREFIX . $id . "/logs/error.log

LogLevel warn
CustomLog /home/hosting/" . MAIN_PREFIX . $id . "/logs/access.log combined

<IfModule mod_php5.c>
php_admin_value doc_root /home/hosting/" . MAIN_PREFIX . $id . "/htdocs/
php_admin_value upload_tmp_dir /home/hosting/" . MAIN_PREFIX . $id . "/tmp/
</IfModule>
</VirtualHost>
";

    $sqry = "UPDATE vhosts SET container='" . $vhost_container . "', active='1', owner_id='" . $owner_id . "', expdate='" . $expdate . "', mysql_password='" . $password . "', ftp_password='" . $password . "' WHERE id='" . $id . "';";
    mysql_query($sqry);


///EMAIL TO CUSTOMER 
    $message = "
Credentials to access your hosting: http://" . $website_i_domain . "
Notice: site will be available in a few minutes after creating.
============================
Domain name: " . $master_domain . "

FTP
======
FTP account host: " . HOST_NAME . "
FTP user name: " . MAIN_PREFIX . $id . "
FTP password: " . $password . "

Upload your WWW to htdocs directory.

MYSQL
======
MySQL database name: " . MAIN_PREFIX . $id . "
MySQL user name: " . MAIN_PREFIX . $id . "
MySQL password: " . $password . "

Manage database using PhpMyAdmin: http://" . HOST_NAME . "/addons/pma

Kind regards,
" . HOSTING_TITLE;

    mail($email, "Access data for " . $master_domain . " on " . HOST_NAME, $message, "From: " . ADMIN_ADDRESS . "");
// end EMAIL 


///EMAIL TO ADMIN 
    $message = "

Created new website:
User : " . $username . "

=========

Credentials to access your hosting: http://" . $website_i_domain . "
Notice: site will be available in a few minutes after creating.
============================
Domain name: " . $master_domain . "

FTP
======
FTP account host: " . HOST_NAME . "
FTP user name: " . MAIN_PREFIX . $id . "
FTP password: " . $password . "

Upload your WWW to htdocs directory.

MYSQL
======
MySQL database name: " . MAIN_PREFIX . $id . "
MySQL user name: " . MAIN_PREFIX . $id . "
MySQL password: " . $password . "

Manage database using PhpMyAdmin: http://" . HOST_NAME . "/addonds/pma

Kind regards,
" . HOSTING_TITLE;

    mail(ADMIN_ADDRESS, "NEW WEBSITE ADDED: " . $master_domain, $message, "From: " . ADMIN_ADDRESS . "");
// end EMAIL 

//update virtual hosts config in Apache

    //exec("/etc/apache2/update_vhosts");
    exec("mysql --user=root --pass=".DBPASSWD." --batch --raw --silent --database=Websites --execute=\"SELECT container FROM vhosts WHERE active = 1 ORDER BY id\" --skip-column-names --protocol=socket --socket=/var/run/mysqld/mysqld.sock > /etc/apache2/sites-enabled/vhosts.conf");
    exec('echo "/usr/bin/sudo /etc/init.d/apache2 reload" | /usr/bin/at now'); //reload apache config (special rights)

//copy new site
    exec("cp -pr /home/mrhost/scripts/site_models/new_site/* /home/hosting/" . MAIN_PREFIX . $id . "/htdocs/");
    exec("chmod 777 -R /home/hosting/" . MAIN_PREFIX . $id . "/htdocs/");

// EDIT INDEX
    $config_path = "/home/hosting/" . MAIN_PREFIX . $id . "/htdocs/index.html";
    $config_data = file_get_contents($config_path);
    $config_data_new = str_replace("domain.com", $master_domain, $config_data);
    file_put_contents($config_path, $config_data_new);

}

function website_destroy($user_id, $master_domain)
{
    mysql_select_db(DBNAME);
    $last_id = mysql_fetch_assoc(mysql_query("SELECT * FROM vhosts WHERE servername='" . $master_domain . "'"));
    $id = $last_id["id"];
    $sqry = "DELETE FROM vhosts_info WHERE vhost_id='" . $id . "'";
    mysql_query($sqry); // delete domain control

    mysql_select_db("ftpd");
    $ftp_dir = "/home/hosting/" . MAIN_PREFIX . $id;
    system("chmod 777 -R " . $ftp_dir);
    system("rm -rf " . $ftp_dir);

    $sqry = "DELETE FROM users WHERE user='" . MAIN_PREFIX . $id . "';";
    mysql_query($sqry); //delete ftp user

    mysql_query("DROP DATABASE " . MAIN_PREFIX . $id . ";"); //delete mysql database

    mysql_select_db("mysql");
    mysql_query("DELETE FROM user WHERE user = '" . MAIN_PREFIX . $id . "';");
    mysql_query("flush privileges;");

//delete vhost 
    mysql_select_db(DBNAME);
    $sqry = "DELETE FROM vhosts WHERE id='" . $id . "';";
    mysql_query($sqry);

///EMAIL TO ADMIN 
    $message = "
" . $master_domain . "
has been DELETED.

Sincerely yours, 
".HOSTING_TITLE;

    mail(ADMIN_ADDRESS, "WEBSITE DELETED: " . $master_domain, $message, "From: " . ADMIN_ADDRESS . "");
// end EMAIL 

//update virtual hosts config in Apache
    exec('mysql --user=root --pass=' . DBPASSWD . ' --batch --raw --silent --database=Websites --execute="SELECT container FROM vhosts WHERE active = 1 ORDER BY id" --skip-column-names --protocol=socket --socket=/var/run/mysqld/mysqld.sock > /etc/apache2/sites-enabled/vhosts.conf');
    exec('echo "/usr/bin/sudo /etc/init.d/apache2 reload" | /usr/bin/at now'); //reload apache config (special rights)

}


function website_backup($id)
{


//delete backup files
    $ftp_dir = "/home/hosting/" . MAIN_PREFIX . $id . "/";


    $backup_file_name = "backup.tar.bz2";
    $backup_file = $ftp_dir . $backup_file_name;
    $backup_tmp = $ftp_dir . "backup.tar";

    $mysqldump_file_name = "mysqldump.sql.gz";
    $mysqldump_file = $ftp_dir . $mysqldump_file_name;
    exec("rm " . $backup_file);
    exec("rm " . $mysqldump_file);

//create mysql database dump 
    exec("/usr/bin/mysqldump -u" . DBUSER . " -p" . DBPASSWD . " " . MAIN_PREFIX . $id . " | /bin/gzip --best > " . $mysqldump_file);

//pack ftp_dir/htdocs
    $backup_file_dir = $ftp_dir . "htdocs";
    exec("cd " . $ftp_dir);
    exec("tar -cf " . $backup_tmp . " " . $backup_file_dir);
    exec("bzip2 " . $backup_tmp);

    exec("chmod 777 " . $backup_file);
    exec("chmod 777 " . $mysqldump_file);

    $files["backup_name"] = $backup_file_name;
    $files["backup_size"] = filesize($backup_file);
    $files["mysqldump_name"] = $mysqldump_file_name;
    $files["mysqldump_size"] = filesize($mysqldump_file);
    return $files;

}

?>