<?php
class reg extends App
{

    function init()
    {
        $this->appInit(get_class($this));

        if(RECAPTCHA_PRIVATE_KEY AND RECAPTCHA_PUBLIC_KEY) { //if enabled
            require_once("recaptchalib.php");
            $resp = null;
            $error = null;
            $this->_tpl->assignArray(array(
                "CAPTCHA" => recaptcha_get_html(RECAPTCHA_PUBLIC_KEY, $error)
            ));
        } else {
            $this->_tpl->clearSection("recaptcha", $_GET["1"]);
        }

        $login = strtolower(clean(@$_POST["login"]));
        $login = preg_replace("/[^a-zA-Z0-9\s]/", "", $login);

        $pay_code = @$_GET["3"] ? @$_GET["3"] : @$_POST["pay_code"];
        $rowpaycode = mysql_fetch_assoc(mysql_query("select * from pay_codes WHERE code='" . $pay_code . "'"));
        $user_id_provider = $rowpaycode["user_id_provider"] ? $rowpaycode["user_id_provider"] : "1";
        $ref = @$_GET["2"] ? @$_GET["2"] : $user_id_provider;

        $this->_tpl->assignArray(array(
            "REF" => $ref,
            "PAY_CODE" => $pay_code,
        ));

        $_chk = new AppCheck();
        $this->_tpl->clearSection("logged_out", "header");

        if (!$_POST) {
            $this->_tpl->clearSection("is_error", $_GET["1"]);
            $this->_tpl->clearSection("no_error", $_GET["1"]);
        }

        if (@$_POST["sendform"]) {

            $this->_tpl->assignArray(array(
                "LOGIN" => $login,
                "USERNAME" => clean($_POST["username"]),
                "EMAIL" => clean($_POST["email"])
            ));


            if(RECAPTCHA_PRIVATE_KEY AND RECAPTCHA_PUBLIC_KEY) { //if enabled
                        if ($_POST["recaptcha_response_field"]) {
                            $resp = recaptcha_check_answer(RECAPTCHA_PRIVATE_KEY,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);
                            if (!$resp->is_valid) {
                                $_chk->add_msg(ERR_CAPTCHA);
                            }
                        } else {
                            $_chk->add_msg(ERR_CAPTCHA);
                        }
            }

            if (!$login) {
                $_chk->add_msg(ERR_LOGIN);
            }
            if (!$_POST["password"]) {
                $_chk->add_msg(ERR_PASSWORD);
            }

            $_chk->check_email($_POST["email"]);

            if (!$_POST["username"]) {
                $_chk->add_msg(ERR_USERNAME);
            }

            if (mysql_fetch_assoc(mysql_query("SELECT * FROM users WHERE login='" . $login . "';")) OR $login == "removed") {
                $_chk->add_msg(ERR_NOT_UNIQUE_LOGIN);
            }

            $balance_plus = mysql_fetch_assoc(mysql_query("SELECT * FROM pay_codes WHERE code='" . $pay_code . "' AND user_id_accept=0;"));
            $balance_plus = $balance_plus["balance"] ? $balance_plus["balance"] : 0;
            $_chk->parse_msg($this->_tpl, "row_MSG", "MESSAGES");

            if ($_chk->messages == false) { //IF ERRORS NOT PRESENT
                $this->_tpl->clearSection("is_error", $_GET["1"]);
                $sqry = "INSERT INTO users SET login='" . $login .
                    "', password='" . clean($_POST["password"]) .
                    "', email='" . clean($_POST["email"]) .
                    "', username='" . clean($_POST["username"]) .
                    "', balance='" . $balance_plus .
                    "', user_parent_id='" . $ref .
                    "', ip='" . $_SERVER["REMOTE_ADDR"] . "', active=1;";
                mysql_query($sqry);
                $last_id = mysql_fetch_assoc(mysql_query("SELECT id FROM users WHERE login='" . $login . "'"));
                $sqry = "UPDATE pay_codes SET user_id_accept='" . $last_id["id"] . "' WHERE code='" . $pay_code . "';";
                mysql_query($sqry); // update pay code (disable after action)
///EMAIL TO REGISTRANT

                $recipient = clean($_POST["email"]);
                $message = "Thank you for register on " . HOSTING_TITLE . ". Now please login to your account on http://" . HOST_NAME;
                mail($recipient, HOSTING_TITLE . ": Welcome to your new account $login", $message, "From: \"" . HOSTING_TITLE . "\" <no-reply@" . HOST_NAME . ">\nContent-type: text/html; charset=utf-8\n");
// end EMAIL


///EMAIL TO admin
                $message = "
New Host Customer.

Login: " . $login . "
Name: " . clean($_POST["username"]);
                mail(ADMIN_ADDRESS, "Registration: " . $login . " " . clean($_POST["username"]), $message, "From: \"Automatic message\" <no-reply@" . HOST_NAME . ">\nContent-type: text/html; charset=utf-8\n");
//end EMAIL


                $this->_tpl->clearSection("is_error", $_GET["1"]);
                $this->_tpl->clearSection("main", $_GET["1"]);


                //auto-login
                $_SESSION["user_id"] = $last_id["id"];
                $_SESSION["login"] = strtolower($login);
                $_SESSION["password"] = md5($_POST["password"]);


                if ($rowpaycode["user_id_provider"]) { //if paycode enabled
                    header("Location: /website_add");
                    exit;
                } else { // if non, then add funds
                    header("Location: /balance_add_direct");
                    exit;
                }

            }


            $this->_tpl->clearSection("no_error", $_GET["1"]);
        }

    }
}

?>