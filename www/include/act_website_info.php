<?php
class website_info extends App
{
    function init()
    {
        $this->appInit(get_class($this));
        $id = $_GET["2"];
        $sqry = "SELECT vhosts.*,DATE_FORMAT(vhosts.expdate, \"%b %d, %Y\") as exp_date, DATE_FORMAT(vhosts_info.vhost_exp_date, \"%b %d, %Y\") as domain_exp_date, vhosts_info.google_pr, vhosts_info.yandex_pr, vhosts_info.alexa_pr FROM vhosts LEFT JOIN vhosts_info ON vhosts.id=vhosts_info.vhost_id WHERE vhosts.id='" . $id . "'";
        $result = mysql_query($sqry);
        $row = mysql_fetch_assoc($result);

        if (!$row['active']) {
            go_home();
        }
        if ($row["owner_id"] != $_SESSION["user_id"] AND !SUPERVISOR) {
            go_home();
        } // if not owner of this site


        $website_domain = $row["servername"];
        $website_a_domain = "www." . $website_domain;
        $website_domain_cleared = str_replace("\.", "_", $website_domain);
        $website_i_domain = $website_domain_cleared . "." . HOST_NAME;

        $todays_date = date("Y-m-d");
        $exp_date = $row["expdate"];

        $today = strtotime($todays_date);
        $expiration_date = strtotime($exp_date);
        $diff = $expiration_date - $today;
        $until_exp_date = round(($diff / 60 / 60 / 24), 0);

        $this->_tpl->assignArray(array(

            "SITE_ID" => $id,
            "MYSQL_PASSWORD" => $row["mysql_password"],
            "FTP_PASSWORD" => $row["ftp_password"],
            "WEBSITE_DOMAIN" => $website_domain,
            "WEBSITE_INTERNAL_DOMAIN" => $website_i_domain,
            "WEBSITE_ADDITIONAL_DOMAIN" => $website_a_domain,
            "EXP_DATE" => $row["exp_date"],
            "UNTIL_EXP_DATE" => $until_exp_date,
            "DOMAIN_EXP_DATE" => $row["domain_exp_date"],
            "GOOGLE_PR" => $row["google_pr"],
            "YANDEX_PR" => $row["yandex_pr"],
            "ALEXA_PR" => $row["alexa_pr"],
        ));


    }
}
?>