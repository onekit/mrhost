<?php
function urling($categ)
{
    $categ = clearUTF($categ);
    $categ = convert2translit($categ);

    $categ = stripslashes($categ);
    $categ = str_replace(",", "~comma~", $categ);
    $categ = str_replace("_", "~underline~", $categ);
    $categ = str_replace(" ", "_", $categ);
    $categ = str_replace("&", "~and~", $categ);
    $categ = str_replace("/", "~slash~", $categ);
    $categ = str_replace("%", "~percent~", $categ);
    $categ = str_replace("\[", "~left~", $categ);
    $categ = str_replace("\]", "~right~", $categ);

    $categ = str_replace("™", "~trademark~", $categ);
    $categ = str_replace("%E2%84%A2", "~trademark~", $categ);
    $categ = str_replace("®", "~registeredtrademark~", $categ);
    $categ = str_replace("%AE", "~registeredtrademark~", $categ);

    $categ = str_replace("\"", "~doublequote~", $categ);
    $categ = str_replace("\'", "~onequote~", $categ);
    $categ = str_replace("’", "~aquote~", $categ);
    $categ = str_replace("\`", "~bquote~", $categ);
    $categ = str_replace("\?", "~question~", $categ);
    $categ = str_replace("\!", "~exmark~", $categ);
    $categ = str_replace(chr(163), "~pound~", $categ);
    $categ = str_replace("=", "~issign~", $categ);
    $categ = str_replace(":", "~dotdot~", $categ);
    $categ = str_replace("#", "~bar~", $categ);

    $categ = strtolower($categ);
    return $categ;
}

function deurling($categ)
{
    $categ = str_replace("~comma~", ",", $categ);
    $categ = str_replace("_", " ", $categ);
    $categ = str_replace("~underline~", "_", $categ);
    $categ = str_replace("~and~", "&", $categ);
    $categ = str_replace("~slash~", "/", $categ);
    $categ = str_replace("~percent~", "%", $categ);
    $categ = str_replace("~left~", "\[", $categ);
    $categ = str_replace("~right~", "\]", $categ);

    $categ = str_replace("~trademark~", "™", $categ);
    $categ = str_replace("~registeredtrademark~", "®", $categ);

    $categ = str_replace("~doublequote~", "\"", $categ);
    $categ = str_replace("~onequote~", "'", $categ);
    $categ = str_replace("~aquote~", "’", $categ);
    $categ = str_replace("~question~", "\?", $categ);
    $categ = str_replace("~exmark~", "\!", $categ);
    $categ = str_replace("~bquote~", "\`", $categ);
    $categ = str_replace("~pound~", chr(163), $categ);
    $categ = str_replace("~issign~", "=", $categ);
    $categ = str_replace("~dotdot~", ":", $categ);
    $categ = str_replace("~bar~", "#", $categ);

    $categ = stripslashes($categ);

    return $categ;
}

function convert_to_valid($text)
{
    $text = stripslashes($text);
    $text = str_replace("\n", "<br/>", $text);
    $text = str_replace("& ", "&amp; ", $text);
    $text = str_replace("AT&T", "AT&amp;T", $text);

//    $text = $text;

    return $text;
}


function clean($text)
{
    $text = stripslashes($text);
    $text = strip_tags($text);
    $text = str_replace("\n", "<br/>", $text);
    $text = str_replace("& ", "&amp; ", $text);
    $text = str_replace("AT&T", "AT&amp;T", $text);
    $text = str_replace("\"", "&quot;", $text);
//    $text = $text;

    return $text;
}


function clearbody($bodytext)
{


    return $bodytext;

}


function utf8_substr($str, $start)
{
    preg_match_all("/./su", $str, $ar);

    if (func_num_args() >= 3) {
        $end = func_get_arg(2);
        return join("", array_slice($ar[0], $start, $end));
    } else {
        return join("", array_slice($ar[0], $start));
    }
}


function number_pad($number, $n)
{
    return str_pad((int)$number, $n, "0", STR_PAD_LEFT);
}


function convert2translit($text)
{

    $text = strtr($text, array(
        "а" => "a",
        "А" => "a",
        "б" => "b",
        "Б" => "b",
        "в" => "v",
        "В" => "v",
        "г" => "g",
        "Г" => "g",
        "д" => "d",
        "Д" => "d",
        "е" => "e",
        "Е" => "e",
        "ё" => "yo",
        "Ё" => "yo",
        "ж" => "zh",
        "Ж" => "zh",
        "З" => "z",
        "з" => "z",
        "и" => "i",
        "И" => "i",
        "й" => "y",
        "Й" => "y",
        "к" => "k",
        "К" => "k",
        "л" => "l",
        "Л" => "l",
        "м" => "m",
        "М" => "m",
        "н" => "n",
        "Н" => "n",
        "о" => "o",
        "О" => "o",
        "п" => "p",
        "П" => "p",
        "р" => "r",
        "Р" => "r",
        "с" => "s",
        "С" => "s",
        "т" => "t",
        "Т" => "t",
        "у" => "u",
        "У" => "u",
        "ф" => "f",
        "Ф" => "f",
        "х" => "h",
        "Х" => "h",
        "ц" => "ts",
        "Ц" => "ts",
        "ч" => "ch",
        "Ч" => "ch",
        "ш" => "sh",
        "Ш" => "sh",
        "щ" => "sch",
        "Щ" => "sch",
        "ъ" => "-",
        "Ъ" => "-",
        "ы" => "y",
        "Ы" => "y",
        "ь" => "'",
        "Ь" => "'",
        "э" => "e",
        "Э" => "e",
        "ю" => "yu",
        "Ю" => "yu",
        "я" => "ya",
        "Я" => "ya"

    ));


    return $text;
}


function clearUTF($text)
{

    $text = str_replace("ö", "o", $text);

    $text = htmlentities($text, ENT_COMPAT, 'UTF-8');
    return html_entity_decode($text, ENT_COMPAT, 'UTF-8//TRANSLIT');
}


?>