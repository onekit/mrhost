<?php
class logout extends App
{
    function init()
    {
        $this->appInit(get_class($this));
        $homepage = "http://" . HOST_NAME . "/";
        session_destroy();
        $_SESSION["previous_url"] = "";
        $_SESSION["pre_previous_url"] = "";
        header("Location: " . $homepage);
        exit;

    }
}

?>