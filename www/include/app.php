<?php
require("pear.php");
require("app_tpl.php");
require("app_check.php");

class App extends PEAR
{

    var $_tpl = null;

    function App()
    {
        $this->PEAR();
    }

    function appInit()
    {

        // Create template object
        $this->_tpl = new AppTpl("./templates");

        $this->_tpl->assignArray(array(
            'ACTION' => $_GET["1"],
            'IMAGES' => "/img",
            'KEYWORDS' => KEYWORDS,
            'DESCRIPTION' => DESCRIPTION,
            'TITLE' => PREFIX . TITLE . SUFIX,
            'SUBTITLE' => TITLE,
            "RANDOMIZE" => "a" . substr(md5(rand(0, 1000)), 0, 5)
        ));


        $this->_tpl->defineArray(array(
            "header" => "header.tpl",
            "menu_admin" => "menu_admin.tpl",
            "menu" => "menu.tpl",
            $_GET["1"] => $_GET["1"] . ".tpl",
            "footer" => "footer.tpl",
        ));

        require_once ("authorize.php");
        $this->_tpl->clearSection($_GET["1"], "header");
        $this->_tpl->clearSection($_GET["1"], $_GET["1"]);
        $this->_tpl->clearSection($_GET["1"], "footer");
    }


    function main()
    {
        $this->_tpl->parseTpl("MENU", "menu");
        $this->_tpl->parseTpl("HEADER", "header");
        $this->_tpl->printTpl("HEADER");
        $this->_tpl->parseTpl("MAIN", $_GET["1"]);
        $this->_tpl->printTpl("MAIN");
        $this->_tpl->parseTpl("FOOTER", "footer");
        $this->_tpl->printTpl("FOOTER");

    }
}

?>