<?php
class code_generate extends App
{
    function init()
    {
        $this->appInit(get_class($this));

        $balance_take = STOCK_PRICE;
        @$amount = $_POST["amount"] ? $_POST["amount"] : $balance_take;
        $amount = intval($amount);
        $this->_tpl->assignArray(array(
            "AMOUNT" => $amount
        ));

        $_chk = new AppCheck();

        if (!$_POST) // If nothing post, then NO ERRORS NO RESULTS
        {
            $this->_tpl->clearSection("is_error", $_GET["1"]);
            $this->_tpl->clearSection("no_error", $_GET["1"]);
        }


        if (@$_POST["sendform"]) {


            if (!$_POST["amount"]) {
                $_chk->add_msg(ERR_PAY_CODE_AMOUNT);
            }

            if (BALANCE < $_POST["amount"]) {
                $_chk->add_msg(ERR_BALANCE);
            }


            $pay_code = substr((md5(rand(0, 10000))), 0, 12);
            $codexist = mysql_num_rows(mysql_query("SELECT * FROM pay_codes WHERE code='" . $pay_code . "';"));

            if ($codexist) {
                $_chk->add_msg(ERR_PAY_CODE_UNIQUE);
            }


            $_chk->parse_msg($this->_tpl, "row_MSG", "MESSAGES");


            if ($_chk->messages == false) { //IF ERRORS NOT PRESENT

                $this->_tpl->clearSection("is_error", $_GET["1"]);
                $this->_tpl->clearSection("main", $_GET["1"]);


                $this->_tpl->assignArray(array(
                    "AMOUNT" => $amount,
                    "PAY_CODE" => $pay_code
                ));


                $sqry = "INSERT INTO pay_codes SET code='" . $pay_code . "', balance='" . $amount . "', user_id_provider='" . $_SESSION["user_id"] . "';";
                mysql_query($sqry); // generate code


                $sqry = "UPDATE users SET balance = balance - " . $amount . " WHERE id='" . $_SESSION["user_id"] . "'";
                mysql_query($sqry);

                $notes = "Pay code: " . $pay_code;
                $sqry = "INSERT INTO transactions SET user_id_sender='" . $_SESSION["user_id"] . "', user_id_recipient='0', amount='" . $amount . "', notes='" . $notes . "',trans_date=NOW();";
                mysql_query($sqry);
                //log transaction


                ///EMAIL TO ADMIN
                $message = "
Generated pay code: " . $pay_code . "
";

                mail(ADMIN_ADDRESS, "PAYCODE GENERATED: " . $pay_code, $message, "From: " . ADMIN_ADDRESS . "");
                // end EMAIL


                $row = mysql_fetch_assoc(mysql_query("SELECT * FROM users WHERE id='" . $_SESSION["user_id"] . "'"));
                $email = $row["email"];

                ///EMAIL TO CUSTOMER
                $message = "
Pay code: " . $pay_code . "
Registration URL with " . $amount . " USD on balance: http://" . HOST_NAME . "/code_" . $pay_code . "
";

                mail($email, "Pay code generated on ".HOST_NAME, $message, "From: " . ADMIN_ADDRESS . "");

                // end EMAIL
                // COMPLETED


                //header("Location: /website_info/".$id);
            } else $this->_tpl->clearSection("no_error", $_GET["1"]);


        }


    }
}

?>