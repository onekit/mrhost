<?php
class admin_website_destroy extends App
{
    function init()
    {
        $this->appInit(get_class($this));

        require_once("account_manage.php");

        @$master_domain = $_POST["master_domain"] ? trim($_POST["master_domain"]) : "";
        $result = mysql_query("SELECT * FROM vhosts WHERE servername='" . $master_domain . "'");
        $row = mysql_fetch_assoc($result);


        $user_id = $row["owner_id"];
        $master_domain = $row["servername"];

        $this->_tpl->assignArray(array(
            "SITE_ID" => $row["id"],
            "WEBSITE_DOMAIN" => $master_domain,
        ));

        $_chk = new AppCheck();

        if (!$_POST) // If nothing post, then NO ERRORS NO RESULTS
        {
            $this->_tpl->clearSection("is_error", $_GET["1"]);
            $this->_tpl->clearSection("no_error", $_GET["1"]);
        }


        if (@$_POST["sendform"]) {


            if (!$master_domain) {
                $_chk->add_msg(ERR_WEBSITE_DOMAIN);
            }


            $_chk->parse_msg($this->_tpl, "row_MSG", "MESSAGES");


            if ($_chk->messages == false) { //IF ERRORS NOT PRESENT

                $this->_tpl->clearSection("is_error", $_GET["1"]);
                $this->_tpl->clearSection("main", $_GET["1"]);
                website_destroy($user_id, $master_domain);

                // COMPLETED
            } else $this->_tpl->clearSection("no_error", $_GET["1"]);
        }

    }
}

?>