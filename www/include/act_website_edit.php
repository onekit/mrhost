<?php
class website_edit extends App
{
    function init()
    {
        $this->appInit(get_class($this));

        $id = $_GET["2"];
        $result = mysql_query("SELECT * FROM vhosts WHERE id='" . $id . "'");
        $row = mysql_fetch_assoc($result);
        if ($row["owner_id"] != $_SESSION["user_id"] AND !SUPERVISOR) {
            go_home();
        } // if not owner of this site

        $container = $row["container"];
        $website_domain = $row["servername"];
        $website_a_domain = "www." . $website_domain;
        $website_domain_cleared = str_replace("\.", "_", $website_domain);
        $website_i_domain = $website_domain_cleared . "." . HOST_NAME;

        $this->_tpl->assignArray(array(

            "SITE_ID" => $id,
            "WEBSITE_DOMAIN" => $website_domain,
            "WEBSITE_INTERNAL_DOMAIN" => $website_i_domain,
            "WEBSITE_ADDITIONAL_DOMAIN" => $website_a_domain,

        ));

        $_chk = new AppCheck();

        if (!$_POST) // If nothing post, then NO ERRORS NO RESULTS
        {
            $this->_tpl->clearSection("is_error", $_GET["1"]);
            $this->_tpl->clearSection("no_error", $_GET["1"]);
        }


        if (@$_POST["sendform"]) {


            $dmexist = mysql_num_rows(mysql_query("SELECT * FROM vhosts WHERE servername = '" . $_POST["master_domain"] . "';"));

            if (!$_POST["master_domain"]) {
                $_chk->add_msg(ERR_WEBSITE_DOMAIN);
            } elseif ($dmexist) {
                $_chk->add_msg(ERR_WEBSITE_DOMAIN_NOT_UNIQUE);
            }
            $_chk->parse_msg($this->_tpl, "row_MSG", "MESSAGES");

            if ($_chk->messages == false) { //IF ERRORS NOT PRESENT

                $this->_tpl->clearSection("is_error", $_GET["1"]);
                $this->_tpl->clearSection("main", $_GET["1"]);


                $new_website_domain = $_POST["master_domain"];
                $new_website_a_domain = "www." . $new_website_domain;
                $new_website_domain_cleared = str_replace("\.", "_", $new_website_domain);
                $new_website_i_domain = $new_website_domain_cleared . "." . HOST_NAME;

                $container = str_replace($website_i_domain, $new_website_i_domain, $container);
                $container = str_replace($website_domain, $new_website_domain, $container);
                $container = str_replace($website_a_domain, $new_website_a_domain, $container);


                $sqry = "UPDATE vhosts SET container='" . $container . "', servername='" . $new_website_domain . "' WHERE id='" . $id . "';";
                mysql_query($sqry);


///EMAIL TO ADMIN 
                $message = "
http://" . $website_i_domain . "
changed to 
http://" . $new_website_domain . "

Kind regards,
" . HOSTING_TITLE;

                mail(ADMIN_ADDRESS, "WEBSITE MASTER DOMAIN CHANGED: " . $website_domain, $message, "From: " . ADMIN_ADDRESS . "");
// end EMAIL 

//update virtual hosts config in Apache

                exec("mysql --user=root --pass=".DBPASSWD." --batch --raw --silent --database=Websites --execute=\"SELECT container FROM vhosts WHERE active = 1 ORDER BY id\" --skip-column-names --protocol=socket --socket=/var/run/mysqld/mysqld.sock > /etc/apache2/sites-enabled/vhosts.conf");
                exec('echo "/usr/bin/sudo /etc/init.d/apache2 reload" | /usr/bin/at now'); //reload apache config (special rights)

                header("Location: /website_info/" . $id);
                exit;
            } else $this->_tpl->clearSection("no_error", $_GET["1"]);


        }


    }
}

?>