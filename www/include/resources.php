<?php
define ('KEYWORDS', "web, hosting, mysql, php, shared, fast");
define ('DESCRIPTION', "Web-hosting for your web-sites. 5 GB, Traffic unlimited, 1 MySQL DB, 1 FTP account. ".HOSTING_TITLE);

//App Errors
define("ERR_EMAIL_INVALID", "Wrong email.");
define("ERR_EMAIL_REQUIRED", "Email is required.");
define("ERR_INVALID_PASSWORD", "Wrong password.");
define("ERR_EMPTY_FIELDS", "Fill at least one field.");

//PASSWORD ERRORS
define("ERR_PASSWORD_EMPTY", "Password is required field.");
define("ERR_SOFTWARE_PASSWORD_WRONG", "Wrong password.");
define("ERR_CONFIRM_PASSWORD", "Passwords are not the same.");
define("ERR_PASSWORD_REMINDER", "Please, enter login");
define("ERR_PASSWORD_REMINDER_2", "Login is not exist");

//Register user
define("ERR_LOGIN", "Login is required field.");
define("ERR_USERNAME", "User name is required field.");
define("ERR_NOT_UNIQUE_LOGIN", "Such login already exist.");
define("ERR_PASSWORD", "Password is required field.");
define("ERR_FIRSTNAME", "First Name is required field.");
define("ERR_LASTNAME", "Last Name is required field.");
define("ERR_CAPTCHA", "Wrong CAPTCHA.");

//Website Errors
define("ERR_WEBSITE_DOMAIN", "Please, enter master domain name.");
define("ERR_WEBSITE_DOMAIN_NOT_UNIQUE", "Such domain already exist in database.");

define("ERR_MYSQL_PASSWORD", "MySQL password cannot be empty.");
define("ERR_FTP_PASSWORD", "FTP password cannot be empty.");

define("ERR_BALANCE", "Insufficient funds. <a href=\"/balance_add_direct\">Add credits to you account</a>.");

//balance
define("ERR_PAY_CODE_EMPTY", "Payment code cannot be empty.");
define("ERR_PAY_CODE_WRONG", "Payment code already used or wrong.");
define("ERR_PAY_DEMO", "Demo mode");
define("ERR_PAY_ORDER_EXIST", "Your transaction already processed");
define("ERR_PAY_CODE_AMOUNT", "Please enter desired amount for your pay code");
define("ERR_PAY_CODE_UNIQUE", "Duplicate generating...");

/**********************************************************/
$pTitle = array(
    "index" => HOSTING_TITLE . " - Cool Shared Web Hosting",
    "promo" => "Promotions",
    "reg" => "Registration",
    "account_activate" => "Account activation",
    "password_reminder" => "Forget password?",
    "my_settings" => "My profile",
    "about" => "About Mr.Host Me",
    "help" => "Help",
    "contact" => "Contact Mr.Host Me",
    "terms" => "Terms &amp; Conditions",
    "website_add" => "Add new web-site",
    "website_info" => "Details of {WEBSITE_DOMAIN}",
    "website_backup" => "Backup of {WEBSITE_DOMAIN}",
    "my_websites" => "My web sites",
    "website_edit" => "Edit web-site",
    "website_renew" => "Renew web-site",
    "balance_add" => "Add balance",
    "balance_add_direct" => "Add balance via 2CO",
    "code_generate" => "Pay Code generate",
    "balance_income" => "Balance income",
    "mysql_password_change" => "Change MySQL password",
    "mysql_phpmyadmin" => "Auto-login to PHPMyAdmin",
    "ftp_password_change" => "Change FTP password",
    "admin_websites" => "Admin control: Web-sites",
    "admin_website_destroy" => "Admin control: DESTROY web-site",
    "login" => "Sign in",
    "logout" => "",
    "xml_login" => "",
);

define('PREFIX', '');
define('SUFIX', ' ');
?>