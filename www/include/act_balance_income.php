<?php
class balance_income extends App
{
    function init()
    {
        $this->appInit(get_class($this));

        $this->_tpl->defineDynamic("item", $_GET["1"]);
        $order_fields = array(
            "amount" => "amount",
            "trans_date" => "trans_date",
        );
        if (!array_key_exists(@$_GET["order"], $order_fields)) {
            $_GET["order"] = "trans_date";
            $_GET["dir"] = "DESC";
        }
        $order = $order_fields[$_GET["order"]];

        $dirs = array("ASC", "DESC");
        if (!in_array($_GET["dir"], $dirs)) $_GET["dir"] = $dirs[0];
        $dir = (!strcmp($_GET["dir"], "DESC")) ? $dirs[0] : $dirs[1];

        @$_GET["page"] = (intval($_GET["page"]) <= 0) ? 1 : intval($_GET["page"]);
        $order_by = " ORDER BY " . $order . " " . $_GET["dir"];

        $sqry = "SELECT
transactions.*,
users.id,
users.username
FROM transactions 
LEFT JOIN users ON users.id = user_id_sender
WHERE user_id_recipient = '" . $_SESSION["user_id"] . "' " . $order_by;

        $result = mysql_query($sqry);

        $_GET["page"] = (intval($_GET["page"]) <= 0) ? 1 : intval($_GET["page"]);
        $found = mysql_num_rows($result);
        $pages_quant = ((($found % LINES_PER_PAGE) > 0) ? (floor($found / LINES_PER_PAGE) + 1) : floor($found / LINES_PER_PAGE));
        if ($pages_quant == 0) {
            $pages_quant++;
        }
        $pagenum = ($_GET["page"] > $pages_quant) ? $pages_quant : $_GET["page"];

        $sqry .= " LIMIT " . (($pagenum - 1) * LINES_PER_PAGE) . "," . LINES_PER_PAGE;
        $result = mysql_query($sqry);
        (mysql_num_rows($result) == 0) ? $this->_tpl->clearSection("list", $_GET["1"]) : $this->_tpl->clearSection("not_found", $_GET["1"]);

        $this->_tpl->assignArray(array(
            "FOUND" => $found,
            "PAGES_QUANT" => $pages_quant,
            "PAGE_NUM" => $pagenum,
        ));


        $i = 0;
        while ($row = mysql_fetch_assoc($result)) {

            $i++;
            $this->_tpl->assignArray(array("SERVER_ID" => $row['id'],
                "CNT" => (($i % 2) ? "EFEFEF" : "E0E0E0"),
                "NN" => ($i + (($pagenum - 1) * LINES_PER_PAGE)),
                "AMOUNT" => $row['amount'],
                "USER_SENDER" => $row['username'],
                "TRANS_DATE" => $row["trans_date"],
                "NOTES" => $row["notes"],

            ));
            $this->_tpl->parse('ITEM', ".item");
        }
        //************************* <sort settings> ***************************//

        $this->_tpl->clearSection($_GET["order"], $_GET["1"]);
        $this->_tpl->clearSection(strtolower($dir), $_GET["1"]);
        $this->_tpl->assignArray(array(
            "ORDER" => $_GET["order"],
            "DIR" => $dir,
            "PAGE_NUM" => $pagenum,
            "DIR2" => $_GET["dir"]
        ));

        //************************* </sort settings> ***************************//
        //************** <pager block> *****************//

        $pager_first_from = (($pagenum % PAGES_PER_BLOCK > 0) ? floor($pagenum / PAGES_PER_BLOCK) : floor($pagenum / PAGES_PER_BLOCK) - 1) * PAGES_PER_BLOCK + 1;

        if ($pager_first_from + PAGES_PER_BLOCK * 2 >= $pages_quant) {
            $pager_first_to = $pages_quant;
            $pager_first_from = ($pages_quant - PAGES_PER_BLOCK * 2 <= 0) ? 1 : $pages_quant - PAGES_PER_BLOCK * 2;
        }
        else
        {
            $pager_first_to = $pager_first_from + PAGES_PER_BLOCK - 1;
        }

        $this->_tpl->defineDynamic("pager_first_link_1", $_GET["1"]);
        $this->_tpl->defineDynamic("pager_first_link_2", $_GET["1"]);
        for ($i = $pager_first_from; $i <= $pager_first_to; $i++)
        {
            $this->_tpl->assignArray(array("PAGE_NUMBER" => $i));

            if ($pagenum > $i) {
                $this->_tpl->parse("PAGER_FIRST_LINK_1", ".pager_first_link_1");
            }
            elseif ($pagenum < $i)
            {
                $this->_tpl->parse("PAGER_FIRST_LINK_2", ".pager_first_link_2");
            }
            if ($i >= $pages_quant) break;
        }

        if ($pager_first_to < $pages_quant) {
            $this->_tpl->defineDynamic("pager_last_link", $_GET["1"]);
            $pager_last_from = $pages_quant - PAGES_PER_BLOCK + 1;
            for ($i = $pager_last_from; $i < $pages_quant + 1; $i++)
            {
                $this->_tpl->assignArray(array("PAGE_NUMBER" => $i));
                $this->_tpl->parse("PAGER_LAST_LINK", ".pager_last_link");
            }
        }

        $this->_tpl->assignArray(array(
            "PAGE_NUMBER" => $pagenum,
            "DIR2" => $_GET["dir"],
            "PAGE_BACK" => $pager_first_from - 1,
            "PAGE_FWD" => $pager_first_to + 1
        ));

        if ($pagenum == $pager_first_from) $this->_tpl->clearSection("pager_first_link_1", $_GET["1"]);
        if ($pagenum == $pager_first_to) $this->_tpl->clearSection("pager_first_link_2", $_GET["1"]);
        if (!isset($pager_last_from)) $this->_tpl->clearSection("pager_last", $_GET["1"]);
        if ($pager_first_from == 1) $this->_tpl->clearSection("link_back", $_GET["1"]);
        if ($pager_first_to == $pages_quant) $this->_tpl->clearSection("link_fwd", $_GET["1"]);

        //************** </pager block> *****************//


    }


}

?>