<?php
class mysql_password_change extends App
{
    function init()
    {
        $this->appInit(get_class($this));
        $id = $_GET["2"];
        $result = mysql_query("SELECT * FROM vhosts WHERE id='" . $id . "'");
        $row = mysql_fetch_assoc($result);
        if ($row["owner_id"] != $_SESSION["user_id"] AND !SUPERVISOR) {
            go_home();
        } // if not owner of this site
        $website_domain = $row["servername"];
        $website_a_domain = "www." . $website_domain;
        $website_domain_cleared = str_replace("\.", "_", $website_domain);
        $website_i_domain = $website_domain_cleared . "." . HOST_NAME;

        $this->_tpl->assignArray(array(

            "SITE_ID" => $id,
            "WEBSITE_DOMAIN" => $website_domain,
            "WEBSITE_INTERNAL_DOMAIN" => $website_i_domain,
            "WEBSITE_ADDITIONAL_DOMAIN" => $website_a_domain,
        ));

        $_chk = new AppCheck();

        if (!$_POST) // If nothing post, then NO ERRORS NO RESULTS
        {
            $this->_tpl->clearSection("is_error", $_GET["1"]);
            $this->_tpl->clearSection("no_error", $_GET["1"]);
        }

        if ($_POST["sendform"]) {


            if (!$_POST["mysql_password"]) {
                $_chk->add_msg(ERR_MYSQL_PASSWORD);
            }


            $_chk->parse_msg($this->_tpl, "row_MSG", "MESSAGES");


            if ($_chk->messages == false) { //IF ERRORS NOT PRESENT

                $this->_tpl->clearSection("is_error", $_GET["1"]);
                $this->_tpl->clearSection("main", $_GET["1"]);


                mysql_query("grant CREATE,INSERT,DELETE,UPDATE,SELECT,DROP on " . MAIN_PREFIX . $id . ".* to " . MAIN_PREFIX . $id . "@localhost;");
                //make sure that rights available for user

                $mysql_password = $_POST["mysql_password"];
                $sqry = "set password for " . MAIN_PREFIX . $id . "@localhost = password('" . $mysql_password . "')";
                mysql_query($sqry);
                $sqry = "flush privileges";
                mysql_query($sqry);

                //add mysql password for admin usage (like installation Wordpress and other)
                $sqry = "UPDATE vhosts SET mysql_password='" . $mysql_password . "' WHERE id='" . $id . "';";
                mysql_query($sqry);

                header("Location: /website_info/" . $id);
                exit;
            } else $this->_tpl->clearSection("no_error", $_GET["1"]);


        }


    }
}

?>