<div class="txt"> 
<!-- BEGIN SECTION: is_error -->
			<h1>Error messages:</h1>
			<div class="error">
			<ul>
<!-- BEGIN DYNAMIC BLOCK: row_MSG -->
			<li> {MESSAGES}</li>
<!-- END DYNAMIC BLOCK: row_MSG -->
			</ul>
			</div>
<!-- END SECTION: is_error -->
<!-- BEGIN SECTION: main -->
<h1>Add credits to account</h1>
<br/>
<form action="" method="post" id="pub_form">
<input type="hidden" name="sendform" value="yes"/>
<div class="txt">
<p>Secret Code:</p>
<input type="text" name="pay_code" class="inp" value="{PAY_CODE}"/>
</div>
<br/>
<input type="submit" value="Increase balance" class="button"/>
</form>

<br/>
<!-- BEGIN SECTION: two_checkout -->
<p>Can't wait? <a href="/balance_add_direct">Pay via credit card</a></p>
<!-- END SECTION: two_checkout -->

<!-- END SECTION: main -->
<!-- BEGIN SECTION: no_error -->
<h1>Balance updated</h1>
<p>Your balance now have +{BALANCE_ADDED} USD.</p>
<br/><br/>
<input type="button" value="Continue" class="button" onclick="parent.location='/'"/>
<!-- END SECTION: no_error -->                                                                       
</div>