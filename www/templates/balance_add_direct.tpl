<div class="txt"> 
<!-- BEGIN SECTION: is_error -->
			<h1>Error messages:</h1>
			<div class="error">
			<ul>
<!-- BEGIN DYNAMIC BLOCK: row_MSG -->
			<li> {MESSAGES}</li>
<!-- END DYNAMIC BLOCK: row_MSG -->
			</ul>
			</div>
<!-- END SECTION: is_error -->
<!-- BEGIN SECTION: main -->
<h1>Refill account balance</h1>
<br/>
<div class="txt">
<!-- BEGIN SECTION: two_checkout -->
<form action="https://www.2checkout.com/2co/buyer/purchase" method="post">
<input type="hidden" name="sid" value="{TWO_CHECKOUT_SID}"/>
<input type="hidden" name="quantity" value="1"/>
<input type="hidden" name="product_id" value="{TWO_CHECKOUT_PRODUCT_ID}"/>
<p>Quantity <input name="quantity" type="text" class="inptlt" size="3" value="1"/> x ${STOCK_PRICE} (Annual hosting price)</p>
<br/>
<input name="submit" type="submit" value="Buy from 2CO" class="button"/>
<br/><br/>
<img src="{IMAGES}/2co.gif" alt="Pay via 2checkout service" /><br/>
<p>2CheckOut.com Inc. (Ohio, USA) is an authorized retailer for goods and services provided by {HOSTING_TITLE}. </p>
</form>
<br/>
<!-- END SECTION: two_checkout -->

<a href="/balance_add">Have a secret code?</a>

</div>

<!-- END SECTION: main -->
<!-- BEGIN SECTION: no_error -->
<h1>Balance updated</h1>
<p>Your balance now have +{BALANCE_ADDED} USD.</p>
<br/><br/>
<input type="button" value="Continue to web-sites page" class="button" onclick="parent.location='/my_websites'"/>
<!-- END SECTION: no_error -->                                                                       
</div>