﻿<div class="txt">		
<!-- BEGIN SECTION: list -->
<p>Found: {FOUND} items. - Page: {PAGE_NUM} from {PAGES_QUANT}.</p>
<!-- END SECTION: list -->
	
<h1>Web sites</h1>
	

	<div class="mywebsites">

<!-- BEGIN SECTION: not_found -->
<div class="txt"><p>Empty list of web-sites. Let's add new web-site.</p></div>
<!-- END SECTION: not_found -->
	
<!-- BEGIN SECTION: list -->
			<div class="tabhead">
			
			<div class="null"></div>

	



<div class="hsitename">

<a href="/my_websites/{CATEGORY_ID}?order=servername&amp;dir={DIR}&amp;page={PAGE_NUM}">Servername</a>

<!-- BEGIN SECTION: expdate -->
<!-- BEGIN SECTION: asc -->
<IMG src="{IMAGES}/i_asc.png" alt="" border="0" hspace="5"/>
<!-- END SECTION: asc -->
<!-- BEGIN SECTION: desc -->
<IMG src="{IMAGES}/i_desc.png" alt="" border="0" hspace="5"/>
<!-- END SECTION: desc -->
<!-- END SECTION: expdate -->

 
</div>
	
			<div class="hexpdate">
	

<a href="/my_websites/{CATEGORY_ID}?order=expdate&amp;dir={DIR}&amp;page={PAGE_NUM}">Exp.date</a>
<!-- BEGIN SECTION: servername -->
<!-- BEGIN SECTION: asc -->
<IMG src="{IMAGES}/i_asc.png" alt="asc"/>
<!-- END SECTION: asc -->
<!-- BEGIN SECTION: desc -->
<IMG src="{IMAGES}/i_desc.png" alt="desc"/>
<!-- END SECTION: desc -->
<!-- END SECTION: servername -->
				
				
			</div>
				
				
			
			




			
			
</div>
<!-- BEGIN DYNAMIC BLOCK: item -->


			<div class="site">
			
				<div class="num">
					<h3>{NN}</h3>
				</div>
				
				<div class="sitename">
					<div class="siteinf">
						<a href="/website_info/{SERVER_ID}">{SERVERNAME}</a>
						<div class="domain_exp">{DOMAIN_EXP_DATE}</div>
					</div>
				</div>
				
			
				
				<div class="showset">
					<a href="/website_info/{SERVER_ID}"><img src="{IMAGES}/i_settings.png" alt="Show settings" /></a>
				</div>
				
				<div class="showset">
					<a href="/mysql_phpmyadmin/{SERVER_ID}" target="_blank"><img src="{IMAGES}/i_mydb.png" alt="Manage MySQL Database" /></a>
				</div>				
				
				<div class="expdate">
						<h2>{EXP_DATE}</h2>
				</div>
				
			</div>





<!-- END DYNAMIC BLOCK: item -->


<!-- BEGIN SECTION: pagging -->
<div class="txt"> 
	<div class="pages">
	Pages:
<!-- BEGIN SECTION: link_back -->
<a class="pagelink" href="/my_websites/{CATEGORY_ID}?order={ORDER}&amp;dir={DIR2}&amp;page={PAGE_BACK}">&laquo;</a>
<!-- END SECTION: link_back -->
<!-- BEGIN SECTION: pager_first_link_1 -->
<!-- BEGIN DYNAMIC BLOCK: pager_first_link_1 -->
<a class="pagelink" href="/my_websites/{CATEGORY_ID}?order={ORDER}&amp;dir={DIR2}&amp;page={PAGE_NUMBER}">{PAGE_NUMBER}</a>&nbsp;
<!-- END DYNAMIC BLOCK: pager_first_link_1 -->
<!-- END SECTION: pager_first_link_1 -->
<!-- BEGIN SECTION: pager_first_no_link -->
&nbsp;{PAGE_NUMBER}&nbsp;
<!-- END SECTION: pager_first_no_link -->
<!-- BEGIN SECTION: pager_first_link_2 -->
<!-- BEGIN DYNAMIC BLOCK: pager_first_link_2 -->
<a class="pagelink" href="/my_websites/{CATEGORY_ID}?order={ORDER}&amp;dir={DIR2}&amp;page={PAGE_NUMBER}">{PAGE_NUMBER}</a>
<!-- END DYNAMIC BLOCK: pager_first_link_2 -->
<!-- END SECTION: pager_first_link_2 -->
<!-- BEGIN SECTION: pager_last -->
&nbsp;...&nbsp;
<!-- BEGIN DYNAMIC BLOCK: pager_last_link -->
<a class="pagelink" href="/my_websites/{CATEGORY_ID}?order={ORDER}&amp;dir={DIR2}&amp;page={PAGE_NUMBER}">{PAGE_NUMBER}</A>
<!-- END DYNAMIC BLOCK: pager_last_link -->

<!-- END SECTION: pager_last -->
<!-- BEGIN SECTION: link_fwd -->
<a class="pagelink" href="/my_websites/{CATEGORY_ID}?order={ORDER}&amp;dir={DIR2}&amp;page={PAGE_FWD}">&raquo;</a>
<!-- END SECTION: link_fwd -->
	</div>
</div>
<!-- END SECTION: pagging -->


			


<!-- END SECTION: list -->



			

	
	
	
	</div>

</div>
