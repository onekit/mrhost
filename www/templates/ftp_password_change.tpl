<div class="txt"> 
<!-- BEGIN SECTION: is_error -->
			<h2>Error messages:</h2>
			<div class="error">
			<ul>
<!-- BEGIN DYNAMIC BLOCK: row_MSG -->
			<li> {MESSAGES}</li>
<!-- END DYNAMIC BLOCK: row_MSG -->
			</ul>
			</div>
<!-- END SECTION: is_error -->
<!-- BEGIN SECTION: main -->
<h1>Change FTP password for user {MAIN_PREFIX}{SITE_ID}</h1>
<p>
[<a href="/website_info/{SITE_ID}">{WEBSITE_DOMAIN}</a>]<br/><br/></p>
<form action="" method="post" id="pub_form">
<input type="hidden" name="sendform" value="yes"/>
<div class="txt">
		<p>New FTP password: </p>
		<input type="text" class="inp" name="ftp_password" value=""/>
<br/><br/>
<input type="submit" value="Change" class="button"/>
</div>
</form>
<!-- END SECTION: main -->
<!-- BEGIN SECTION: no_error -->
<h1>FTP password has been updated</h1>
<p>You can access to your FTP account with following credentials:<br/></p>
<ul>
<li>FTP host: {HOST_NAME}</li>
<li>FTP user: {MAIN_PREFIX}{SITE_ID}</li>
<li>FTP password: <i>hidden</i></li>
</ul>
<!-- END SECTION: no_error -->
</div>