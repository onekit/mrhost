﻿<div class="txt">		
<!-- BEGIN SECTION: list -->
<p>Found: {FOUND} items. - Page: {PAGE_NUM} from {PAGES_QUANT}.</p>
<!-- END SECTION: list -->
<h1>Transactions</h1>
	<div class="mywebsites">
			<div class="tabhead">
<!-- BEGIN SECTION: not_found -->
<p>Empty list of transactions</p>
<!-- END SECTION: not_found -->
	
<!-- BEGIN SECTION: list -->
<div class="hsitename">
<a href="/balance_income?order=amount&amp;dir={DIR}&amp;page={PAGE_NUM}">Amount</a>
<!-- BEGIN SECTION: trans_date -->
<!-- BEGIN SECTION: asc -->
<IMG src="{IMAGES}/i_asc.png" alt="" border="0" hspace="5"/>
<!-- END SECTION: asc -->
<!-- BEGIN SECTION: desc -->
<IMG src="{IMAGES}/i_desc.png" alt="" border="0" hspace="5"/>
<!-- END SECTION: desc -->
<!-- END SECTION: trans_date -->
</div>
	
<div class="hexpdate">
<a href="/balance_income?order=trans_date&amp;dir={DIR}&amp;page={PAGE_NUM}">Date</a>
<!-- BEGIN SECTION: amount -->
<!-- BEGIN SECTION: asc -->
<IMG src="{IMAGES}/i_asc.png" alt="asc"/>
<!-- END SECTION: asc -->
<!-- BEGIN SECTION: desc -->
<IMG src="{IMAGES}/i_desc.png" alt="desc"/>
<!-- END SECTION: desc -->
<!-- END SECTION: amount -->
			</div>
</div>
<!-- BEGIN DYNAMIC BLOCK: item -->


			<div class="site">
			
				<div class="num">
					<h3>{NN}</h3>
				</div>
				
				<div class="amount">
					<h2>From: {USER_SENDER}</h2>
					<h1>{AMOUNT} USD</h1>	
					<p>{NOTES}</p>
					
				</div>
				
			
			
					
	
				
			
				
				<div class="trans_date">
						<h2>{TRANS_DATE}</h2>
				</div>
				
			</div>





<!-- END DYNAMIC BLOCK: item -->


<!-- BEGIN SECTION: pagging -->
<div class="txt"> 
	<div class="pages">
	Pages:
<!-- BEGIN SECTION: link_back -->
<a class="pagelink" href="/balance_income?order={ORDER}&amp;dir={DIR2}&amp;page={PAGE_BACK}">&laquo;</a>
<!-- END SECTION: link_back -->
<!-- BEGIN SECTION: pager_first_link_1 -->
<!-- BEGIN DYNAMIC BLOCK: pager_first_link_1 -->
<a class="pagelink" href="/balance_income?order={ORDER}&amp;dir={DIR2}&amp;page={PAGE_NUMBER}">{PAGE_NUMBER}</a>&nbsp;
<!-- END DYNAMIC BLOCK: pager_first_link_1 -->
<!-- END SECTION: pager_first_link_1 -->
<!-- BEGIN SECTION: pager_first_no_link -->
&nbsp;{PAGE_NUMBER}&nbsp;
<!-- END SECTION: pager_first_no_link -->
<!-- BEGIN SECTION: pager_first_link_2 -->
<!-- BEGIN DYNAMIC BLOCK: pager_first_link_2 -->
<a class="pagelink" href="/balance_income?order={ORDER}&amp;dir={DIR2}&amp;page={PAGE_NUMBER}">{PAGE_NUMBER}</a>
<!-- END DYNAMIC BLOCK: pager_first_link_2 -->
<!-- END SECTION: pager_first_link_2 -->
<!-- BEGIN SECTION: pager_last -->
&nbsp;...&nbsp;
<!-- BEGIN DYNAMIC BLOCK: pager_last_link -->
<a class="pagelink" href="/balance_income?order={ORDER}&amp;dir={DIR2}&amp;page={PAGE_NUMBER}">{PAGE_NUMBER}</A>
<!-- END DYNAMIC BLOCK: pager_last_link -->

<!-- END SECTION: pager_last -->
<!-- BEGIN SECTION: link_fwd -->
<a class="pagelink" href="/balance_income?order={ORDER}&amp;dir={DIR2}&amp;page={PAGE_FWD}">&raquo;</a>
<!-- END SECTION: link_fwd -->
	</div>
</div>
<!-- END SECTION: pagging -->


			


<!-- END SECTION: list -->


	</div>

</div>
