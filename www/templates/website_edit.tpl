<div class="txt"> 
<!-- BEGIN SECTION: is_error -->

			<h2>Error messages:</h2>
			<div class="error">
			<ul>
<!-- BEGIN DYNAMIC BLOCK: row_MSG -->
			<li> {MESSAGES}</li>
<!-- END DYNAMIC BLOCK: row_MSG -->
			</ul>
			</div>

<!-- END SECTION: is_error -->
<!-- BEGIN SECTION: main -->
<h1>Edit Web-Site</h1>
<p>
[<a href="/website_info/{SITE_ID}">{WEBSITE_DOMAIN}</a>]<br/><br/>
WARNING! Please make sure that you understand before change domain name.
</p><br/>
<form action="" method="post" id="pub_form">
<input type="hidden" name="sendform" value="yes"/>
<div class="txt">
	
		<p>Master Domain: </p>
	
		<input type="text" class="inp" name="master_domain" value="{WEBSITE_DOMAIN}"/>
	
</div>		
<br/>
<input type="submit" value="Change" class="button"/>

</form>

<!-- END SECTION: main -->
<!-- BEGIN SECTION: no_error -->
<h1>Web-site has been updated</h1>
<p>Thanks.</p>
<!-- END SECTION: no_error -->                                                                       
</div>