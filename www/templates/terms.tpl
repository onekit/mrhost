<h1>GENERAL TERMS AND CONDITIONS OF SERVICE</h1>
<div class="txt">

<h2>1. INTERPRETATION</h2>

<h3>1.1 DEFINITIONS</h3>

<p>In these Conditions the following words and expressions shall have the following meanings:

<p>"This Agreement" means the agreement made between Us and the Client which incorporate these Conditions;

<p>"We, Us" — The {HOSTING_TITLE} group;

<p>"Terms" means these terms and conditions of business;

<p>"Service" or "Services" means the domain name registration services; World Wide Web page hosting;

<p>"the Server" means the computer server equipment operated by We in connection with the provision of the Services;

<p>"Web Site" means the area on the Server allocated by We to Client for use by Client as a site on the Internet;

<p>"Client" or "Clients" means any and all persons, firms, companies and organisations who purchases Services from We and any of their or its employees, consultants and authorised agents.

<p>"Order" means a request made by the Client to us for Services to be supplied subject to these Conditions;

<p>"Spam" means the sending of unsolicited mass email from or through a We Server or using an email address that is maintained on a We Server or the sending of unsolicited mass email with advertisement of a domain name that is maintained on a We Server;

<p>"ISP" means the Internet Service Provider;

<p>"User" or "Users" means any and all persons, firms, companies and organisations who visits the Client's Web Site;

<p>"CGI-SCRIPT" means a program or a script file executed on the Web Site in response to the User request;

<p>"Multimedia Files" means any graphics, audio, and video files;

<p>"Resources" means bandwidth and/or Server processor utilization;

<p>"Third Party" means any and all persons, firms, companies and organisations who is not a party to this Agreement;

<h3>1.2 APPLICATION OF TERMS</h3>

<p>These Terms apply to any or all Services to be provided by We to Client from time to time. These Terms is personal to the Client. In addition to these Terms, all domain name registrations are subject to the terms and conditions of any registrar We may use to fulfil the Order.

<h2>2. SERVICE-SPECIFIC TERMS AND CONDITIONS</h2>

<p>We provides World Wide Web page hosting. We reserve the right to amend and update the Terms at any time without prior notice.

<h2>2.1 THE ACCOUNTS</h2>

<p>All We Web Hosting accounts are payable in advance in one payment. The pay period starts from the day the hosting becomes active. This is not-refundable should the client decide to transfer their domain to another host part way at any time after 30 days trial period. There is no charge for domain name transfer. Clients' first payment wich includes domain name registration fee of $20 is non-cancellable and non-refundable.

<p>On ordering a domain name, client agrees to the terms and conditions as set out by the particular Naming Registry. We reserve the right to change prices at any time. The current prices shall be published at all times at the We Web Site.

<p>No bills or invoices will be sent by regular mail. All bills and invoices are sent by email.

<p>All pricing is guaranteed for the term of the payment. All pricing is in UK Sterling. Other currencies can be selected on the Credit Card Order Form for the information of overseas clients.

<p>If you exceed your webspace allowance you will be notified by email and given the choice of either paying for the extra space or deleting some of your files.

<h2>2.2 SERVER USE</h2>

<p>All sites may be used for personal and commercial use. Client is responsible for the content of their pages, including obtaining the legal permission for any works they include and ensuring that the contents of these pages do not violate any laws.

<p>We servers may be used for lawful purposes only. Transmission, storage, or distribution of any information, data, or material in violation of any applicable law or regulation is prohibited. This includes, but is not limited to: copyrighted material; trademarks; trade secrets or other intellectual property rights used without proper authorization; material that is obscene, defamatory, constitutes an illegal threat, or violates export control laws. Examples of unacceptable content or links include: pirated software, hacker programs or archives, Warez sites, MP3, and IRC bots.

<p>We does not allow adult material, foul or abusive language, Warez, illegal MP3 sites, Slander / Libel, impersonations, promotion of violence or terrorism, racial / sexual / political discrimination or other services we deem inappropriate. The designation of "adult material" is left entirely to the discretion of We. Warez - includes pirated software, ROMS, emulators, phreaking, hacking, password cracking. IP spoofing, etc., and encrypting of any of the above. Also includes any sites which provide "links to" or "how to" information about such material.

<p>Clients accept that server downtime, whether for system upgrades or any other reason, may occur and there will be no compensation paid for any period of downtime in any event.

<p>Any web site that uses a high amount of server resources (such as, but not limited to, CPU time, memory usage, and network resources) will be given the option to either pay additional fees (which will depend on the resources required), reduce the resources used to an acceptable level. We shall be the sole arbiter of what is considered to be a high server usage level.

<p>Any attempts to undermine or cause harm to a We Server or Client are strictly prohibited.

<p>Any web hosting account deemed to be adversely affecting server performance or network integrity will be shut down without prior notice.

<p>We may allow programs to run in the background. These programs will be considered on an individual basis and the Client will incur extra charges based on system resources used and operational maintenance needed. If the Client wish to run background programs the Client must contact We at {SUPPORT_EMAIL} so that We can arrange set-up.

<p>We does not allow IRC or IRC bots to be operated on the Server.

<h2>2.3 CGI-SCRIPTS</h2>

<p>CGI-SCRIPT sharing with domains not hosted by We is not permitted. Any CGI-SCRIPTS deemed to be adversely affecting Server performance or network integrity will be shut down without prior notice.

<h2>2.4 MULTIMEDIA FILES</h2>

<p>We shared web hosting accounts are not to be used for the purposes of distributing and storing unusual amounts of Multimedia Files. Any Web Site whose disk space usage for storing multimedia files exceeds 90% of its total usage, in terms of total size or number of files, will be considered to be using an unusual amount of multimedia files.

<h2>2.5 HIGH RESOURCE POLICY</h2>

<p>When a Web Site is found to be monopolising the Resources available We reserves the right to suspend that Web Site immediately. This policy is only implemented in extreme circumstances and is intended to prevent the misuse of our Servers. Clients may be offered an option whereby We continues hosting the Web Site for an additional fee. We may implement this policy to its sole discretion.

<h2>2.6 SECURITY</h2>

<p>Violations of system or network security are prohibited and may result in criminal and civil liability. Examples include but are not limited to the following: unauthorized access, use, probing, or scanning of systems security or authentication measures, data, or traffic; interference with service to any user, host, or network including, without limitation, mail bombing, flooding, deliberate attempts to overload a system, broadcast attacks; forging of any TCP-IP packet header or any part of the header information in an email or a newsgroup posting.

<h2>2.7 UCE (Unsolicited Commerce Email) - SPAM</h2>

<p>Spam is STRICTLY prohibited. We will be the sole arbiter as to what constitutes a violation of this provision. Cleints are also in violation of this provision if they engage in spamming using the service of another ISP, but channel activities through a We Server as a mail drop for responses. Violators will be assessed a minimum fine of $200 and will face immediate suspension.

<p>Clients must not participate in any form of SPAM.

<h2>2.8 30 DAY TRIAL</h2>

<p>For Clients opening their first account the initial 30 days are available on a trial basis. The Clients will not be entitled to this benefit if they have previously had an account with We unless the period of absence has been greater than one year. New Clients will not be entitled to a trial period if any account has previously been created at the same postal address within the same 12 month time span.

<h2>2.9 CANCELLATION AND REFUNDS</h2>

<p>We reserve the right to suspend or cancel a customer's access to any or all Services at any time without notice. In this event Clients will be entitled to a pro rata refund based upon the remaining period of membership. If the Client contravenes the Terms refunds will be given at the discretion of the Company Management.

<p>Clients' first payment includes domain name registration fee of ${STOCK_PRICE} for the first year. Domain name registration fee will be given at the discretion of the Company Management.

<p>Clients may cancel their account at any time. During the initial trial period Clients who wish to cancel will be entitled to a refund for their hosting fees except domain name registration fee of $20 which is non-cancellable and non-refundable. The refund has to be processed within 30 days.

<p>Any incentives offered to the Clients when opening the account will also be cancelled.

<h2>2.10 PAYMENT OPTIONS</h2>

<p>We accepts Mastercard, Visa (not Visa electron), Switch and Solo.

<h2>2.11 DOMAIN NAMES</h2>

<p>International domain names (.com,.net,.org,.info) are registered for 1 year period. Two years period may be offered as a bonus. Clients must contact Us to activate this bonus.

<h2>3. FORCE MAJEURE</h2>

<p>We shall not be liable for any failure in performing its obligations under this Agreement due to circumstances beyond its reasonable control.

<h2>4. LIMITATION OF LIABILITY AND INDEMNITY</h2>

<p>4.1 We shall not be liable to the Client for any failure by We to attend to any of its obligations or any other matters under this Agreement caused by We or Third Party.

<p>4.2 The Client agrees that it shall defend, indemnify, save and hold We harmless from any and all demands, liabilities, losses, costs and claims, including reasonable attorney's fees asserted against We, its agents, its customers, officers and employees, that may arise or result from any Service provided or performed or agreed to be performed or any product sold by customer, its agents, employees or assigns. Customer agrees to defend, indemnify and hold harmless We against liabilities arising out of; (1) any injury to person or property caused by any products sold or otherwise distributed in connection with the Server; (2) any material supplied by customer infringing or allegedly infringing on the proprietary rights of a third party; (3) copyright infringement and (4) any defective products sold to customer from the Server.

<h2>5. DISCLAIMER</h2>

<p>We will not be responsible for any damages your business may suffer. We makes no warranties of any kind, expressed or implied for Services provided. We disclaims any warranty or merchantability or fitness for a particular purpose. The includes loss of data resulting from delays, nondeliveries, wrong delivery, and any and all Service interruptions caused by We and its employees. We reserves the right to revise the Terms at any time.

<h2>6. GENERAL</h2>

<p>6.1 This Agreement is personal to the Client.

<p>6.2 This Agreement applies to all countries. This Agreement shall be governed by and construed in accordance with the law of Client residing country and the Server hosted country as well.

<p>6.3 We reserves the right to revise This Agreement at any time without notice.





</div>