<!-- BEGIN SECTION: is_error -->
			<h2>Error messages:</h2>
			<div class="error">
			<ul>
<!-- BEGIN DYNAMIC BLOCK: row_MSG -->
			<li>{MESSAGES}</li>
<!-- END DYNAMIC BLOCK: row_MSG -->
			</ul>
			</div>

<!-- END SECTION: is_error -->

<!-- BEGIN SECTION: main -->

<h1>Registration</h1>

<div class="txt">
<br/>
						
<form name="form2" method="post" action="" onsubmit="Subm.disabled='true';">
<input type="hidden" name="sendform" value="yes"/>
<input type="hidden" name="ref" value="{REF}"/>
<input type="hidden" name="pay_code" value="{PAY_CODE}"/>

		<p>Login</p>
		<input type="text" name="login" value="{LOGIN}" class="inp{IS_LOGIN_ERROR}"/>

		
		<p>Password</p>
		<input type="password" name="password" value="" class="inp{IS_PASSWORD_ERROR}"/>
		
	
		<p>Email</p>
		<input type="text" name="email" value="{EMAIL}" class="inp{IS_EMAIL_ERROR}"/>

	
		<p>User Name</p>
		<input type="text" name="username" value="{USERNAME}" class="inp{IS_USERNAME_ERROR}"/>

<br/>
<!-- BEGIN SECTION: recaptcha -->
<br/>
    <script type="text/javascript">
        var RecaptchaOptions = {
        lang : 'en',
        theme: 'white'
        };
    </script>
    {CAPTCHA}
<!-- END SECTION: recaptcha -->
<br><br>
<input type="submit" class="button" value="Sign up" name="Subm">
    <br>
    <br>
    <br>
</form>
<br/>
<p>By clicking &quot;Sign up&quot; you confirm that you agree with our <a href="/terms">terms &amp; conditions</a>.</p>
</div>
<!-- END SECTION: main -->

<!-- BEGIN SECTION: no_error -->
<h1>You're registered</h1>
<p>Thanks for trusting to {HOSTING_TITLE}. The most simple web-hosting.</p>
<!-- END SECTION: no_error -->                                                                       
