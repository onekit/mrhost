<h1>Promotions</h1>
<div class="txt">
<h2>Referral link to registration</h2>
<p>Earn $10 for every ordered web-site by your friend.<br/>
Invite your friends on {HOSTING_TITLE} using this link:</p><br/>
<p>Referral URL:<a href="http://{HOST_NAME}/reg/{USER_ID}">http://{HOST_NAME}/reg/{USER_ID}</a></p>

<h2>Referral link to registration with funds</h2>
<p>If you want to sell hosting personally to your customer and get profit, you can sell special URL with paycode.<br/>
Customer will register using your paycode and automatically get funds on balance. It gives to customer possibility create web-site immediately.<br/>
<a href="/code_generate">Generate Pay Code</a><br/>
Not sold pay codes can be returned to your account via <a href="/balance_add">this form</a></p>
</div>