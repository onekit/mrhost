<div class="websites_info"> 
<!-- BEGIN SECTION: main -->
<div class="setright">
<div class="website_domain">
<h3>Domain</h3>
<ul>
	<li>Master Domain: <b>{WEBSITE_DOMAIN}</b> <a href="/website_edit/{SITE_ID}"><img src="{IMAGES}/i_edit.png" alt="change domain"/></a></li>
	<li>Alias: <b>{WEBSITE_ADDITIONAL_DOMAIN}</b></li>
	<li>Free domain: <b><a href="http://{WEBSITE_INTERNAL_DOMAIN}" target="_blank">{WEBSITE_INTERNAL_DOMAIN}</a></b></li>
</ul>
</div>
<div class="website_ftp">
<h3>FTP</h3>
<ul>
	<li>FTP host: <b>{HOST_NAME}</b></li>
	<li>FTP username: <b>{MAIN_PREFIX}{SITE_ID}</b></li>
	<li>FTP password: <b>{FTP_PASSWORD}</b><a href="/ftp_password_change/{SITE_ID}"><img src="{IMAGES}/i_edit.png" alt="change pass"/></a></li>
</ul>
</div>
<div class="website_mysql">
<h3>MySQL</h3>
<ul>
	<li>MySQL database: <b> {MAIN_PREFIX}{SITE_ID}</b></li>
	<li>MySQL UserName: <b>{MAIN_PREFIX}{SITE_ID}</b></li>
	<li>MySQL user password: <b>{MYSQL_PASSWORD}</b><a href="/mysql_password_change/{SITE_ID}"><img src="{IMAGES}/i_edit.png" alt="change pass"/></a></li>
<li><a href="/mysql_phpmyadmin/{SITE_ID}" target="_blank" class="h3">Manage MySQL Database</a></li>
</ul>
</div>
</div>
<div class="setleft">
<h1>Settings for: &quot;{WEBSITE_DOMAIN}&quot;</h1>
<br/>
<div class="website_stats">
	<ul>
		<li>Hosting expiration date: <b>{EXP_DATE}</b> <em>(in {UNTIL_EXP_DATE} days)</em> [ <a href="/website_renew/{SITE_ID}">Renew</a> ]</li>
		<li>Domain expiration date: <b>{DOMAIN_EXP_DATE}</b></li>
		<li>Google PR: <b>{GOOGLE_PR}</b></li>
		<li>Yandex PR: <b>{YANDEX_PR}</b></li>
		<li>Alexa Rank: <b>{ALEXA_PR}</b></li>
	</ul>
</div>
<div class="website_oper">
<h3>Actions</h3>
	<div class="action">
	<a href="/website_backup/{SITE_ID}"><img src="{IMAGES}/i_backup.png" alt="download the backup of your site" /></a>
	<a href="/website_backup/{SITE_ID}">Backup</a>
	</div>
</div>
</div>
<!-- END SECTION: main -->
</div>