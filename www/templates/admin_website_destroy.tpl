<!-- BEGIN SECTION: is_error -->
			<h2>Error messages:</h2>
			<div class="error">
			<ul>
<!-- BEGIN DYNAMIC BLOCK: row_MSG -->
			<li> {MESSAGES}</li>
<!-- END DYNAMIC BLOCK: row_MSG -->
			</ul>
			</div>
<!-- END SECTION: is_error -->
<!-- BEGIN SECTION: main -->
<h1>DESTROY Web-Site</h1>
<br/>
<div class="txt">
<p>WARNING! This action will destroy MySQL database, all directories, FTP account and all files of this web-site.</p>
<form action="" method="post" id="pub_form">
<input type="hidden" name="sendform" value="yes"/><br/>
<p>Master domain:</p>
<input type="text" name="master_domain" value="{WEBSITE_DOMAIN}" />
<br/><br/>
<input type="submit" value="Destroy" class="button"/>
</form>
</div>
<!-- END SECTION: main -->
<!-- BEGIN SECTION: no_error -->
<h1>Web-site has been deleted</h1>
<p>Thanks.</p>
<!-- END SECTION: no_error -->                                                                       
