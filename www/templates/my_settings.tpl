<div class="txt">
    <!-- BEGIN SECTION: is_error -->
    <h2>Error Messages:</h2>

    <div class="error">
        <ul>
            <!-- BEGIN DYNAMIC BLOCK: row_MSG -->
            <li>{MESSAGES}</li>
            <!-- END DYNAMIC BLOCK: row_MSG -->
        </ul>
    </div>
    <!-- END SECTION: is_error -->
    <!-- BEGIN SECTION: main -->


    <h1>{TITLE}</h1>

    <div class="txt">
        <h3>Information</h3>
        <br/>

        <form name="form2" method="post" action="/my_settings" onsubmit="Subm.disabled='true';">
            <input type="hidden" name="sendform" value="yes"/>


            <p>Email:</p>
            <input type="text" name="email" value="{EMAIL}" class="inp"/>

            <p>Username:</p>
            <input type="text" name="username" value="{USERNAME}" class="inp"/>

            <p>Company name:</p>
            <input type="text" name="companyname" value="{COMPANYNAME}" class="inp"/>

            <p>Address:</p>
            <input type="text" name="address" value="{ADDRESS}" class="inp"/>

            <p>City:</p>
            <input type="text" name="city" value="{CITY}" class="inp"/>

            <p>Zip/Postal:</p>
            <input type="text" name="zip" value="{ZIP}" class="inp"/>

            <p>Country:</p>
            <input type="text" name="country" value="{COUNTRY}" class="inp"/>


            <br><br>
            <input type="submit" value="Save changes" name="Subm" class="button"/>
        </form>


        <br>
        <br><br><br>


        <h3>Change password</h3>

        <br>

        <form name="form3" method="post" action="/my_settings" onsubmit="Sub.disabled='true';">
            <input type="hidden" name="changepass" value="yes"/>


            <p>New password:</p>

            <input type="password" name="password1" value="" class="inp{IS_REGISTER_PASSWORD_ERROR}"/>


            <p>once again:</p>

            <input type="password" name="password2" value="" class="inp{IS_REGISTER_PASSWORD_ERROR}"/>

            <br/><br/>
            <input type="submit" value="Change pass" name="Sub" class="button"/>

        </form>


    </div>
    <!-- END SECTION: main -->


    <!-- BEGIN SECTION: no_error -->
    <h1>Updated</h1>
    <input type="button" class="button" value="Continue" onclick="parent.location='/'"/>
    <!-- END SECTION: no_error -->
</div>