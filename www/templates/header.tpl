<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>{TITLE}</title>
    <meta charset="UTF-8" />
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
    <meta name="viewport" content="width=device-width">
    <meta name="keywords" content="{KEYWORDS}"/>
	<meta name="description" content="{DESCRIPTION}"/>
	<link rel="stylesheet" type="text/css" href="/css/null.css"/>
	<link rel="stylesheet" type="text/css" href="/css/index.css"/>
<!-- BEGIN SECTION: logged_in_usr -->
	<link rel="stylesheet" type="text/css" href="/css/main_int.css"/>
<!-- END SECTION: logged_in_usr -->
<script type="text/javascript" src="/js/common.js"></script>
</head>
<body>
<div id="message" class="message_off"></div>
<div class="maintable">
<div class="center">
<!-- BEGIN SECTION: logged_in_usr -->
<div class="logged_frame">
<div class="greetings">Greetings, <a href="/my_settings">{LOGGED_USERNAME}</a>! </div>

<span class="balance"><img src="{IMAGES}/i_balance2.png" alt="Credits" class="balanceico"/><a href="/balance_income" class="addfunds" onmouseover="show_message('Income transactions history')">Balance: {CREDITS} USD</a></span><br/>

<img src="{IMAGES}/i_addfunds.png" class="i_menu_i" alt="Add funds to your account" /><a href="/balance_add_direct" class="addfunds" onmouseover="show_message('Increase your balance via PayPal, Credit Card or enter Gift Code')">Add funds</a> <br />

<a href="/logout" class="ltl">Log out</a>
</div>
<div class="logo2"></div>
<!-- END SECTION: logged_in_usr -->
<!-- BEGIN SECTION: logged_in_usr -->
<!-- END SECTION: logged_in_usr -->
<div class="top_menu"><img src="{IMAGES}/i_home.png" alt="help and FAQ" class="i_menu_i" /><a href="/">Home</a> <img src="{IMAGES}/i_contacts.png" alt="help and FAQ" class="i_menu_i" /><a href="/contact">Contact</a>  
<!-- BEGIN SECTION: logged_in_usr -->
<img src="{IMAGES}/i_help.png" alt="help and FAQ" class="i_menu_i" /><a href="/help">Help</a>
<!-- END SECTION: logged_in_usr -->
</div>
<!-- BEGIN SECTION: logged_out -->
	<div class="header">
		<div class="logo"></div>
	</div>
<!-- END SECTION: logged_out -->		
<div class="main_cont">
<!-- BEGIN SECTION: logged_out -->
	<div class="right">
		<div class="signup">
            <a href="/reg" class="button">Sign up</a>
        </div>
		<div class="auth">
		<form action="" method="post" onsubmit="try_login(this);return false;">
			<fieldset>
			<p>UserName</p>
			<input type="text" name="login" class="authinp" value="username" onfocus="this.value=this.value!='username'?this.value:''" onblur="this.value=this.value?this.value:'username'"/>
			<p>Password</p>
			<input type="password" name="password" class="authinp" value="password" onfocus="this.value=''" onblur="this.value=this.value?this.value:'password'"/>

				
			
			
			<div class="b_login">
				<input type="submit" src="{IMAGES}/b_login.png" value="Log on" class="button"/>
			</div>
			
			
			
			<br/>
			<a href="/password_reminder" class="ltl">Forget password?</a>
			
			</fieldset>
		</form>
		</div>
	</div>
<!-- END SECTION: logged_out -->
		
	<div class="left">
