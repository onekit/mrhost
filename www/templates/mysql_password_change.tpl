<div class="textblock"> 
<!-- BEGIN SECTION: is_error -->
			<h2>Error messages:</h2>
			<div class="error">
			<ul>
<!-- BEGIN DYNAMIC BLOCK: row_MSG -->
			<li> {MESSAGES}</li>
<!-- END DYNAMIC BLOCK: row_MSG -->
			</ul>
			</div>
<!-- END SECTION: is_error -->
<!-- BEGIN SECTION: main -->
<h1>Change MySQL password for user {MAIN_PREFIX}{SITE_ID}</h1>
<p>[<a href="/website_info/{SITE_ID}">{WEBSITE_DOMAIN}</a>]<br/><br/>
<form action="" method="post" id="pub_form">
<input type="hidden" name="sendform" value="yes"/>
<div class="txt">
		<p>New MySQL password: </p>
	<div class="txt">
		<input type="text" name="mysql_password" value="" class="inp"/>
	</div>
</div>		
<br/>
<input type="submit" value="Change" class="button"/>
</form>
<!-- END SECTION: main -->
<!-- BEGIN SECTION: no_error -->
<h1>MySQL password has been updated</h1>
<p>You can access to your database via <a href="http://{HOST_NAME}/addons/pma" target="_blank">http://{HOST_NAME}/addons/pma</a> with login <b>{MAIN_PREFIX}{SITE_ID}</b> and choosen password.</p>
<!-- END SECTION: no_error -->                                                                       
</div>