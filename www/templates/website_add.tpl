<div class="txt">
<!-- BEGIN SECTION: is_error -->

			<h2>Error messages:</h2>
			
			<ul class="error">
<!-- BEGIN DYNAMIC BLOCK: row_MSG -->
			<li> {MESSAGES}</li>
<!-- END DYNAMIC BLOCK: row_MSG -->
			</ul>
			
<!-- END SECTION: is_error -->
<!-- BEGIN SECTION: main -->
<h1>Add new web-site</h1>
<br/>

<form action="" method="post" id="pub_form">
<input type="hidden" name="sendform" value="yes"/>
<div class="formfields">
	
	<p>Master Domain or one-word site name:</p>
	
	<br/>
		<input type="text" class="inp" name="master_domain" value="{WEBSITE_DOMAIN}"/><br/>
	<span class="ltl">Example: google</span>
	<br/>
	
	<br/>
	<p>
	Annual subscription on {HOSTING_TITLE} costs {STOCK_PRICE} USD.<br/>
	That funds will be charged from your balance.<br/>
	
	</p>
	<span class="ltl">End of subscription: {WEBSITE_EXPDATE}</span>
</div>		
<br/>
<input type="submit" value="Create" class="button"/>

</form>

<!-- END SECTION: main -->
<!-- BEGIN SECTION: no_error -->
<h1>Web-site has been added</h1>
<p>Please check your email and save received document.</p>
<br/><br/>
<input type="button" value="Continue" class="button" onclick="parent.location='/website_info/{SITE_ID}'"/>

<!-- END SECTION: no_error -->                                                                       
</div>