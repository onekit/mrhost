<div class="txt">
<!-- BEGIN SECTION: not_found -->
<p>Empty list of web-sites. Let's add new web-site.</p>
<!-- END SECTION: not_found -->
<!-- BEGIN SECTION: list -->
<p>Found: {FOUND} items. - Page: {PAGE_NUM} from {PAGES_QUANT}.</p>
<!-- END SECTION: list -->
	</div>
<h1>Admin Web sites</h1>
	<div class="mywebsites">
			<div class="tabhead">
			<div class="null"></div>
<!-- BEGIN SECTION: list -->


<div class="hadmin_sitename">



<a href="/admin_websites?order=servername&amp;dir={DIR}&amp;page={PAGE_NUM}">Servername</a>

<!-- BEGIN SECTION: owner -->
<!-- BEGIN SECTION: expdate -->


<!-- BEGIN SECTION: asc -->
<img src="{IMAGES}/i_asc.png" alt="asc" border="0"/>
<!-- END SECTION: asc -->

<!-- BEGIN SECTION: desc -->
<img src="{IMAGES}/i_desc.png" alt="desc" border="0"/>
<!-- END SECTION: desc -->


<!-- END SECTION: expdate -->
<!-- END SECTION: owner -->

</div>


<div class="hadmin_expdate">

<a href="/admin_websites?order=expdate&amp;dir={DIR}&amp;page={PAGE_NUM}">
		Exp.date
</a>

<!-- BEGIN SECTION: owner -->
<!-- BEGIN SECTION: servername -->

<!-- BEGIN SECTION: asc -->
<IMG src="{IMAGES}/i_asc.png" alt="asc" border="0"/>
<!-- END SECTION: asc -->

<!-- BEGIN SECTION: desc -->
<IMG src="{IMAGES}/i_desc.png" alt="desc" border="0"/>
<!-- END SECTION: desc -->

<!-- END SECTION: servername -->
<!-- END SECTION: owner -->


</div>



<div class="hadmin_owner">

<a href="/admin_websites?order=owner&amp;dir={DIR}&amp;page={PAGE_NUM}">
		Owner
</a>

<!-- BEGIN SECTION: expdate -->
<!-- BEGIN SECTION: servername -->

<!-- BEGIN SECTION: asc -->
<IMG src="{IMAGES}/i_asc.png" alt="asc" border="0"/>
<!-- END SECTION: asc -->

<!-- BEGIN SECTION: desc -->
<IMG src="{IMAGES}/i_desc.png" alt="desc" border="0"/>
<!-- END SECTION: desc -->

<!-- END SECTION: servername -->
<!-- END SECTION: expdate -->


</div>





</div>

<!-- BEGIN DYNAMIC BLOCK: item -->

			<div class="site">

				<div class="num">
					<h3>{NN}</h3>
				</div>

				<div class="sitename">
					<div class="siteinf">
						<a href="/website_info/{SERVER_ID}">{SERVERNAME}</a>
						<div class="domain_exp">{DOMAIN_EXP_DATE}</div>
					</div>
				</div>



				<div class="showset">
					<a href="/website_info/{SERVER_ID}"><img src="{IMAGES}/i_settings.png" alt="Show settings" /></a>
				</div>

				<div class="expdate">
						<h2>{EXP_DATE}</h2>
						<p>{OWNER_NAME}</p>
				</div>


			</div>





<!-- END DYNAMIC BLOCK: item -->





<!-- BEGIN SECTION: pagging -->
<div class="txt">
	<div class="pages">
	Pages:
<!-- BEGIN SECTION: link_back -->
<a class="pagelink" href="/admin_websites?order={ORDER}&amp;dir={DIR2}&amp;page={PAGE_BACK}">&laquo;</a>
<!-- END SECTION: link_back -->
<!-- BEGIN SECTION: pager_first_link_1 -->
<!-- BEGIN DYNAMIC BLOCK: pager_first_link_1 -->
<a class="pagelink" href="/admin_websites?order={ORDER}&amp;dir={DIR2}&amp;page={PAGE_NUMBER}">{PAGE_NUMBER}</a>&nbsp;
<!-- END DYNAMIC BLOCK: pager_first_link_1 -->
<!-- END SECTION: pager_first_link_1 -->
<!-- BEGIN SECTION: pager_first_no_link -->
&nbsp;{PAGE_NUMBER}&nbsp;
<!-- END SECTION: pager_first_no_link -->
<!-- BEGIN SECTION: pager_first_link_2 -->
<!-- BEGIN DYNAMIC BLOCK: pager_first_link_2 -->
<a class="pagelink" href="/admin_websites?order={ORDER}&amp;dir={DIR2}&amp;page={PAGE_NUMBER}">{PAGE_NUMBER}</a>
<!-- END DYNAMIC BLOCK: pager_first_link_2 -->
<!-- END SECTION: pager_first_link_2 -->
<!-- BEGIN SECTION: pager_last -->
&nbsp;...&nbsp;
<!-- BEGIN DYNAMIC BLOCK: pager_last_link -->
<a class="pagelink" href="/admin_websites?order={ORDER}&amp;dir={DIR2}&amp;page={PAGE_NUMBER}">{PAGE_NUMBER}</A>
<!-- END DYNAMIC BLOCK: pager_last_link -->

<!-- END SECTION: pager_last -->
<!-- BEGIN SECTION: link_fwd -->
<a class="pagelink" href="/admin_websites?order={ORDER}&amp;dir={DIR2}&amp;page={PAGE_FWD}">&raquo;</a>
<!-- END SECTION: link_fwd -->
	</div>
</div>
<!-- END SECTION: pagging -->

<!-- END SECTION: list -->


</div>