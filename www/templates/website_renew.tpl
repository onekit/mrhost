<div class="txt">
<!-- BEGIN SECTION: is_error -->

			<h2>Error messages:</h2>
			<div class="error">
			<ul>
<!-- BEGIN DYNAMIC BLOCK: row_MSG -->
			<li> {MESSAGES}</li>
<!-- END DYNAMIC BLOCK: row_MSG -->
			</ul>
			</div>

<!-- END SECTION: is_error -->
<!-- BEGIN SECTION: main -->
<h1>Renew Web-Site</h1>

<p>[<a href="/website_info/{SITE_ID}">{WEBSITE_DOMAIN}</a>]<br/><br/>
WARNING! This action will take {STOCK_PRICE} USD from your balance and add 1 year of subscription.</p>
<form action="" method="post" id="pub_form">
<input type="hidden" name="sendform" value="yes"/>
<br/>
<input type="submit" value="Renew" class="button"/>

</form>

<!-- END SECTION: main -->
<!-- BEGIN SECTION: no_error -->
<h1>Web-site has been renewed</h1>
<p>Thanks.</p>
<!-- END SECTION: no_error -->                                                                       
</div>