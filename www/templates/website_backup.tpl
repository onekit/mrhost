<div class="txt">
<!-- BEGIN SECTION: is_error -->

			<h2>Error messages:</h2>
			
			<ul class="error">
<!-- BEGIN DYNAMIC BLOCK: row_MSG -->
			<li> {MESSAGES}</li>
<!-- END DYNAMIC BLOCK: row_MSG -->
			</ul>
			
<!-- END SECTION: is_error -->
<!-- BEGIN SECTION: main -->
<h1>Make MySQL dump and FTP files to archive of &quot;{WEBSITE_DOMAIN}&quot;</h1>
<br/>

<form action="" method="post" id="pub_form">
<input type="hidden" name="sendform" value="yes"/>

	<p>
	Backup will be available in your FTP directory after creation<br/>
	<br/>
	
	</p>
<br/>
<input type="submit" value="Make backup" class="b_lite"/>

</form>

<!-- END SECTION: main -->
<!-- BEGIN SECTION: no_error -->
<h1>Backup has been created</h1>
<br/>
<p>Now <a href="{LINK_BACKUP_FILENAME}" class="h2">{BACKUP_FILENAME}</a> ({BACKUP_FILESIZE} Mb) and <a href="{LINK_MYSQLDUMP_FILENAME}" class="h2">{MYSQLDUMP_FILENAME}</a> ({MYSQLDUMP_FILESIZE} Mb) available for download from your FTP directory</p>
<br/>
<input type="button" class="button" value="Continue" onclick="parent.location='/website_info/{SITE_ID}'"/>
<!-- END SECTION: no_error -->                                                                       
</div>