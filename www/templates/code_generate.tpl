<div class="txt"> 
<!-- BEGIN SECTION: is_error -->

			<h1>Error messages:</h1>
			<div class="error">
			<ul>
<!-- BEGIN DYNAMIC BLOCK: row_MSG -->
			<li> {MESSAGES}</li>
<!-- END DYNAMIC BLOCK: row_MSG -->
			</ul>
			</div>

<!-- END SECTION: is_error -->
<!-- BEGIN SECTION: main -->
<h1>Generate Pay Code</h1>
<br/>
<form action="" method="post" id="pub_form">
<input type="hidden" name="sendform" value="yes"/>
<div class="txt">
	
	<p>This operation will take funds from your balance:</p>
	<input type="text" name="amount" class="inp" value="{AMOUNT}"/>

</div>		
<br/>
<input type="submit" value="Create Pay Code" class="button"/>

</form>
<br/>



<!-- END SECTION: main -->
<!-- BEGIN SECTION: no_error -->
<h1>Pay Code generated</h1>
<p>Amount of pay code: {AMOUNT} USD. Registration URLs has been sent to your email.<br/>
Pay Code: <b>{PAY_CODE}</b>
</p>
<br/><br/>
<input type="button" value="Continue" class="button" onclick="parent.location='/'"/>
<!-- END SECTION: no_error -->                                                                       
</div>