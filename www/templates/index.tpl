﻿<!-- BEGIN SECTION: logged_in_usr -->
<div class="blarge">
	<div class="bicon">
		<a href="/my_websites"><img src="{IMAGES}/i_b_home.png" alt="My Web-Sites" class="biconimg"  onmouseover="show_message('Listing of your web-sites.')"/></a>
	</div>
	<div class="btext">
		<div class="btext2">
			<a href="/my_websites" class="btextlink">My Websites</a>
		</div>
		<div class="bdesc">
			List of web-sites, web-site settings, <br>manage MySQL database,<br> access to FTP
		</div>
	</div>
</div>


<div class="blarge2">
	<div class="bicon">
		<a href="/website_add"><img src="{IMAGES}/i_b_addsite.png" alt="Add Web-Site" class="biconimg" onmouseover="show_message('Create new web-site')"/></a>
	</div>
	<div class="btext">
		<div class="btext2">
			<a href="/website_add" class="btextlink2">Add Web-Site</a>
		</div>
		<div class="bdesc">
			Create new web-site in one click, just type web-site name and click &quot;create&quot;
		</div>
	</div>
</div>



<div class="blarge">
	<div class="bicon">
		<a href="/my_settings"><img src="{IMAGES}/i_b_profile.png" alt="My Profile" class="biconimg" onmouseover="show_message('Profile settings (User name, Email, Password)')"/></a>
	</div>
	<div class="btext">
		<div class="btext2">
			<a href="/my_settings" class="btextlink">My Profile</a>
		</div>
		<div class="bdesc">
			Change password, full name, email
		</div>
	</div>
</div>



<div class="blarge2">
	<div class="bicon">
		<a href="/promo"><img src="{IMAGES}/i_b_promo.png" alt="My Promotions" class="biconimg" onmouseover="show_message('Earn some money via friends invitation.')"/></a>
	</div> 
	<div class="btext">
		<div class="btext2">
			<a href="/promo" class="btextlink2">My Promotions</a>
		</div>
		<div class="bdesc">
			Earn money with our referral program
		</div>
	</div>
</div>

<!-- END SECTION: logged_in_usr -->

<!-- BEGIN SECTION: logged_out -->
<h1>Welcome to most simple web-hosting</h1>
<br/>
		<p><strong>Web site</strong> in a few clicks. Only <strong>{STOCK_PRICE} USD</strong> for annual subscription including:</p>

		<ul>
			<li>5GB disk space</li>
			<li>PHP5, MySQL5 supported</li>
			<li>Unlimited traffic</li>
			<li>1 MySQL database</li>
			<li>1 FTP account</li>
		</ul>

<!-- END SECTION: logged_out -->
		
