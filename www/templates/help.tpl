<h1>User Manual</h1>
<br/>
<div class="txt">
<h1>DNS settings</h1>
<h2>Gossimer</h2>
<p>1. Login to Gossimer account<br/>
2. Click "My account".<br/>
3. Pick from drop-down Menu "Domains" - "List All Order"<br/>
4. Choose required domain from list of domains. <br/>
5. Select "DNS Free" and click "Manage DNS".<br/>
6. In popup window press button "Add A Record"<br/>
7. In field Host Name type any host which you need to be available on your web-site, <br/>
	Destination IPv4 Address: {HOST_IP}</p>

<h2>GoDaddy</h2>
<p>1. Login to GoDaddy account<br/>
2. Click on menu Domains.<br/>
3. On left menu click on Domain Manager.<br/>
4. Click on required domain from list.<br/>
5. Go to Total DNS Control.<br/>
6. Add or Edit A record:</p>

	<p>Host: @<br/>
	Points To: {HOST_IP}<br/>
	TTL: 1 Hour
	</p>

</div>
<br/>
<div class="txt">
<p>Support Email: <a href="mailto:onekit@gmail.com">onekit@gmail.com</a></p>
</div>
