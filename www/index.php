<?php
require_once("config/config.php");
if (!@mysql_connect(DBHOST,DBUSER,DBPASSWD)) {
    echo "Database error";
    exit;
}
mysql_select_db(DBNAME);
mysql_query("SET NAMES utf8");
require_once("include/app.php");
unset($_GET);
$url = parse_url($_SERVER["REQUEST_URI"]);
$urlParts = explode("/", $url['path']);
@parse_str($url['query'], $vars);
foreach ($urlParts as $part) { //parse pages
    $_GET[] = $part;
}
foreach ($vars as $k => $v) { //parse vars
    $_GET[$k] = $v;
}
if (!isset($_GET["1"])) {$_GET["1"]="index";}
session_start();
require_once("include/resources.php");
require_once("include/acl.php");
require_once("include/act_".$_GET["1"].".php");
$appObj = new $_GET["1"];
$appObj->App();
$appObj->init();
$appObj->main();
mysql_close();
?>