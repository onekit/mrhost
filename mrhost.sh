#!/bin/sh
apt-get -y install sudo
apt-get -y install at
apt-get -y install git
apt-get -y install apache2
apt-get -y install php5
apt-get -y install php5-mysql
apt-get -y install php5-memcached
apt-get -y install pure-ftpd-mysql
apt-get -y install memcached
apt-get -y install whois
cd /home/
mkdir -p hosting
chmod 777 hosting
git clone https://bitbucket.org/onekit/mrhost.git
cd mrhost
echo "www-data ALL= NOPASSWD: /etc/init.d/apache2 reload" >> /etc/sudoers
cp /home/mrhost/scripts/etc/at.allow /etc/
cp /home/mrhost/scripts/etc/at.deny /etc/
rm /etc/apache2/sites-enabled/*
cp /home/mrhost/scripts/etc/apache2/sites-enabled/default /etc/apache2/sites-enabled/
cp /home/mrhost/scripts/etc/apache2/sites-enabled/mrhost /etc/apache2/sites-enabled/
cp /home/mrhost/scripts/etc/apache2/sites-enabled/vhosts.conf /etc/apache2/sites-enabled/
chmod 755 /etc/apache2/sites-enabled/vhosts.conf
chmod 777 /etc/pure-ftpd/db/mysql.conf
rm /etc/pure-ftpd/db/mysql.conf
cp /home/mrhost/scripts/etc/pure-ftpd/db/mysql.conf  /etc/pure-ftpd/db/
cp /home/mrhost/scripts/etc/pure-ftpd/auth/65unix  /etc/pure-ftpd/auth/65unix
cp /home/mrhost/scripts/etc/pure-ftpd/conf/ChrootEveryone /etc/pure-ftpd/conf/
cp /home/mrhost/scripts/etc/pure-ftpd/conf/CreateHomeDir /etc/pure-ftpd/conf/
chmod 777 /etc/pure-ftpd/db/mysql.conf
rm /etc/cron.daily/mrhost_whois_updates
cp /home/mrhost/scripts/etc/cron.daily/mrhost_whois_updates /etc/cron.daily/
rm /etc/cron.daily/mrhost_notifications
cp /home/mrhost/scripts/etc/cron.daily/mrhost_notifications /etc/cron.daily/
chmod 755 /etc/cron.daily/mrhost_whois_updates
chmod 755 /etc/cron.daily/mrhost_notifications
cp /home/mrhost/ip/www/install/logo.png /home/mrhost/www/img/
chmod 777 /home/mrhost/ip/www/install/index.php
chmod 777 /home/mrhost/www/config
chmod 777 /home/mrhost/www/sessions
chmod 777 -R /etc/apache2/sites-enabled
a2enmod rewrite
a2enmod mem_cache
apt-get -y install mysql-server
apache2ctl restart