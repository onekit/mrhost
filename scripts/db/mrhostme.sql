/*!40101 SET SQL_MODE=''*/;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`Websites` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `Websites`;
/*Table structure for table `pay_codes` */
DROP TABLE IF EXISTS `pay_codes`;
CREATE TABLE `pay_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `user_id_accept` int(11) DEFAULT '0',
  `user_id_provider` int(11) NOT NULL DEFAULT '1',
  `balance` varchar(32) DEFAULT '50',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `transactions` */
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id_sender` int(11) DEFAULT NULL,
  `user_id_recipient` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `trans_date` datetime DEFAULT NULL,
  `order_num` varchar(64) DEFAULT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*Table structure for table `users` */
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `balance` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `user_parent_id` int(11) NOT NULL DEFAULT '1',
  `ip` varchar(15) NOT NULL,
  `notes` text,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `zip` varchar(32) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `companyname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT  INTO `users`(`id`,`login`,`password`,`username`,`email`,`balance`,`active`,`user_parent_id`,`ip`,`notes`,`address`,`city`,`zip`,`country`,`companyname`) VALUES (1,'admin','admin','Aliaksandr Harbunou','onekit@gmail.com','500',1,1,'',NULL,'','','','','MrHostME');
/*Table structure for table `vhosts` */
DROP TABLE IF EXISTS `vhosts`;
CREATE TABLE `vhosts` (
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `container` text NOT NULL,
  `servername` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `expdate` date NOT NULL,
  `mysql_password` varchar(255) DEFAULT NULL,
  `ftp_password` varchar(255) DEFAULT NULL,
  `notify` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `active` (`active`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*Table structure for table `vhosts_info` */
DROP TABLE IF EXISTS `vhosts_info`;
CREATE TABLE `vhosts_info` (
  `vhost_id` int(11) NOT NULL,
  `whois_data` text,
  `vhost_exp_date` date DEFAULT NULL,
  `google_pr` varchar(64) DEFAULT NULL,
  `yandex_pr` varchar(64) DEFAULT NULL,
  `alexa_pr` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`vhost_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*Table structure for table `vhosts_log` */
DROP TABLE IF EXISTS `vhosts_log`;
CREATE TABLE `vhosts_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vhost_id` int(11) DEFAULT NULL,
  `alexa_pr` varchar(128) DEFAULT NULL,
  `yandex_pr` varchar(128) DEFAULT NULL,
  `google_pr` varchar(128) DEFAULT NULL,
  `logdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

/*!40101 SET NAMES utf8 */;
/*!40101 SET SQL_MODE=''*/;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ftpd` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ftpd`;
/*Table structure for table `users` */
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user` varchar(30) NOT NULL,
  `password` varchar(64) NOT NULL,
  `home` varchar(128) NOT NULL,
  `bandwidth_limit_upload` smallint(5) NOT NULL DEFAULT '0',
  `bandwidth_limit_download` smallint(5) NOT NULL DEFAULT '0',
  `ip_allow` varchar(15) NOT NULL DEFAULT 'any',
  `quota` smallint(5) NOT NULL DEFAULT '0',
  `quota_files` int(11) NOT NULL DEFAULT '0',
  `active` enum('yes','no') NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`user`),
  UNIQUE KEY `User` (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
