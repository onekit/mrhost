<?php
require_once("../www/config/config.php");
if (!@mysql_connect(DBHOST, DBUSER, DBPASSWD)) {
    echo "Edit www/config/config.php";
    exit;
}
mysql_select_db(DBNAME);
mysql_query("SET NAMES utf8");
require_once("../www/include/google_pagerank.php");
require_once("../www/include/yandex_pagerank.php");
require_once("../www/include/alexa_pagerank.php");
$sqry = "SELECT
vhosts.*, 
vhosts_info.*
FROM vhosts
LEFT JOIN vhosts_info ON id=vhost_id;";
$result = mysql_query($sqry);
$i = 0;
while ($row = mysql_fetch_assoc($result)) {
// get host name from URL
    preg_match('@^(?:http://)?([^/]+)@i', $row["servername"], $matches);
    $host = $matches[1];
// get last two segments of host name
    preg_match('/[^.]+\.[^.]+$/', $host, $matches);
    $master_domain = $matches[0];
    $google_pr = getpagerank($master_domain);
    $yandex_pr = getcy($master_domain);
    $alexa = getAlexaData($master_domain);
    $alexa_pr = $alexa["POPULARITY"];
    $sqry = "UPDATE vhosts_info SET google_pr='" . $google_pr . "', yandex_pr='" . $yandex_pr . "', alexa_pr='" . $alexa_pr . "' WHERE vhost_id='" . $row["id"] . "';";
    mysql_query($sqry);
    //log
    $sqry = "INSERT INTO vhosts_log SET vhost_id='" . $row["id"] . "', logdate=NOW(), google_pr='" . $google_pr . "', yandex_pr='" . $yandex_pr . "', alexa_pr='" . $alexa_pr . "';";
    mysql_query($sqry);
}
?>