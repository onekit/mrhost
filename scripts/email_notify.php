<?php
require_once("../www/config/config.php");
if (!@mysql_connect(DBHOST,DBUSER,DBPASSWD)) {
    echo "Edit www/config/config.php";
    exit;
}
mysql_select_db(DBNAME);
mysql_query("SET NAMES utf8");
require_once("../www/config/config.php");
$sqry = "SELECT
DATE_FORMAT(expdate, \"%b %D, %Y\") as exp_date,
vhosts.*,
users.username,
users.email
FROM 
vhosts 
LEFT JOIN users ON owner_id = users.id
WHERE expdate < (NOW() + INTERVAL 7 DAY) AND notify='0'";
$result = mysql_query($sqry);
$i = 0;
while ($row = mysql_fetch_assoc($result)) {
    $id = $row["id"];
    $website = strtoupper($row["servername"]);
    $email = $row["email"];
    $message = "Hello, " . $row["username"] . "!
This is automatic notification message from " . HOST_NAME . " web-hosting provider.
Your web-site " . $website . " will be expired in a few days (" . $row["exp_date"] . ").
Please login to your account on http://" . HOST_NAME . "
and RENEW your annual web-hosting subscription in menu \"My Web-Sites\" - \"" . $website . "\"

Thank you in advance.
Best regards,
".HOSTING_TITLE;

    mail($email, "Notification: " . $website . " on " . HOST_NAME, $message, "From: " . ADMIN_ADDRESS . "");

    //update notification flag
    $sqry = "UPDATE vhosts SET notify='1' WHERE id='" . $id . "';";
    mysql_query($sqry);
}

?>