<?php
$serverhost = "{SERVERHOST}";
$serverip = "{SERVERIP}";

// Database connection parameters
define("DBTYPE", "mysql"); // Database type
define("DBNAME", "Websites"); // Database name
define("DBHOST", "localhost"); // Database host
define("DBUSER", "root"); // Username
define("DBPASSWD", "{DBPASSWD}"); // Password

define ("SUPPORT_EMAIL", "{SUPPORT_EMAIL}");
define ("HOSTING_TITLE", "{HOSTING_TITLE}");
define ("MAIN_PREFIX", "site"); // please do not change after installation 
define ("HOST_NAME", $serverhost ? $serverhost : $_SERVER["HTTP_HOST"]);
define ("HOST_IP", $serverip ? $serverip : $_SERVER['SERVER_ADDR']);


//ReCaptcha https://www.google.com/recaptcha/admin/create
define ("RECAPTCHA_PUBLIC_KEY", "");
define ("RECAPTCHA_PRIVATE_KEY", "");

//2Checkout merchant
//Register
define ("TWO_CHECKOUT_SID", ""); //2co account sid
define ("TWO_CHECKOUT_PRODUCT_ID", ""); //2co product id
//Create product in 2Checkout account
//Approved URL: http://domain.com/balance_add_direct
//Your domain name instead of domain.com
define ("STOCK_PRICE", "{STOCK_PRICE}"); //annual web hosting price

define("MAIL_OUTPUTTER_ADDRESS", "App Log system <" . SUPPORT_EMAIL . ">");
define("ADMIN_ADDRESS", '"' . HOSTING_TITLE . '" <' . SUPPORT_EMAIL . '>');

//Error handling and logging
ini_set("error_reporting", E_ALL);
ini_set("display_errors", 0);
ini_set("display_startup_errors", 0);
ini_set("log_errors", 1);
ini_set("track_errors", 1);
ini_set("error_log", "../logs/error.log");

//Session settings
ini_set("session.gc_probability", 0);
ini_set("session.gc_maxlifetime", "28800");
ini_set("session.auto_start", "1");
ini_set("session.cookie_lifetime", 0);
ini_set("session.gc_divisor", "100");
ini_set("session.use_cookies", 1);
ini_set("session.use_only_cookies", 0);
ini_set("session.cookie_path", "/");
ini_set("session.cache_expire", 180);
if (class_exists('Memcache')) {
    $session_save_path = "tcp://127.0.0.1:11211?persistent=1&weight=1&timeout=1&retry_interval=15";
    ini_set('session.save_handler', 'memcache');
    ini_set('session.save_path', $session_save_path);
} else {
    ini_set('session.save_handler', 'files');
    ini_set('session.save_path', "sessions");
}
define("LINES_PER_PAGE", 50);
define("PAGES_PER_BLOCK", 5);
?>