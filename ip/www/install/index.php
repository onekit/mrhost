<?php
if ($_POST) {

    if ($_POST["mysql_root_password"] AND $_POST["support_email"] AND $_POST["hosting_title"]) { //Install Database
        exec("mysql -uroot -p" . $_POST['mysql_root_password'] . " < /home/mrhost/scripts/db/mrhostme.sql");

        $file = file_get_contents("/home/mrhost/ip/www/install/config.php");
        $file = str_replace("{SERVERHOST}", $_POST["hostname"], $file);
        $file = str_replace("{SERVERIP}", $_POST["serverip"], $file);
        $file = str_replace("{HOSTING_TITLE}", $_POST["hosting_title"], $file);
        $file = str_replace("{DBPASSWD}", $_POST["mysql_root_password"], $file);
        $file = str_replace("{SUPPORT_EMAIL}", $_POST["support_email"], $file);
        $file = str_replace("{STOCK_PRICE}", $_POST["stock_price"], $file);
        file_put_contents("/home/mrhost/www/config/config.php", $file);

        $file = file_get_contents("/etc/pure-ftpd/db/mysql.conf"); //Configure FTPD
        $file = str_replace("MyPassword", $_POST["mysql_root_password"], $file);
        file_put_contents("/etc/pure-ftpd/db/mysql.conf", $file);
    }


    if ($_POST["admin_email"] AND $_POST["hostname"]) { //Apache2 VirtualHosts
        $file = file_get_contents("/etc/apache2/sites-enabled/mrhost"); //main web-site
        $file = str_replace("onekit@gmail.com", $_POST["admin_email"], $file);
        $file = str_replace("mrhost.me", $_POST["hostname"], $file);
        file_put_contents("/etc/apache2/sites-enabled/mrhost", $file);

        $file = file_get_contents("/etc/apache2/sites-enabled/default"); //default web-site
        $file = str_replace("mrhost.me", $_POST["hostname"], $file);
        file_put_contents("/etc/apache2/sites-enabled/default", $file);

    }
    exec('echo "/usr/bin/sudo /etc/init.d/apache2 reload" | /usr/bin/at now');
    header("Location: http://" . $_POST["hostname"] . "/");
    exit;


} else {
    ?>
<!DOCTYPE html>
<html>
<head>
    <title>Installation MrHost Engine</title>
    <style>
        body {
            margin: 40px;
        }

        h1 {
            font: normal 24px Arial;
        }

        pre, p {
            font: normal 14px Arial;
            text-indent: 10px;
            margin: 0;
        }

        input {
            border: solid 1px #c0c0c0;
            border-radius: 15px;
            font: bold 18px Arial;
            padding: 15px;
        }

        input[readonly=readonly] {
            background: #c0c0c0;
            color: #fff;
        }
    </style>
</head>
<body>
<h1>Installation MrHost engine</h1>

<p>This is the last step of installation process after running batch</p>
<pre>

        <form action="" method="post">
            Hostname
            <input type="text" name="hostname" value="<?=$_SERVER["HTTP_HOST"]?>">

            IP
            <input type="text" name="serverip" readonly="readonly" value="<?=$_SERVER["SERVER_ADDR"]?>">

            MySQL Password
            <input type="text" name="mysql_root_password" value=""/>

            Administrator Email
            <input type="text" name="admin_email" value=""/>

            Support Email
            <input type="text" name="support_email" value=""/>

            Hosting Title
            <input type="text" name="hosting_title" value=""/>

            Annual Hosting Price (USD)
            <input type="text" name="stock_price" value="50"/>

            <input type="submit" value="Install">
        </form>
</pre>
</body>
</html>
<?
}
?>